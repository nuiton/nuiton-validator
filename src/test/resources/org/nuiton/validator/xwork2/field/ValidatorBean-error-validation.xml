<!--
  #%L
  Nuiton Validator
  %%
  Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<!DOCTYPE validators PUBLIC "-//Apache Struts//XWork Validator 1.0.3//EN"
        "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

    <field name="stringValue">
        <field-validator type="requiredstring" short-circuit="true">
            <message>stringValue.required</message>
        </field-validator>
    </field>

    <field name="existingFile">
        <field-validator type="requiredFile" short-circuit="true">
            <message>existingFile.required</message>
        </field-validator>
        <field-validator type="existingFile" short-circuit="true">
            <message>existingFile.not.exist</message>
        </field-validator>
    </field>

    <field name="notExistingFile">
        <field-validator type="requiredFile" short-circuit="true">
            <message>notExistingFile.required</message>
        </field-validator>
        <field-validator type="notExistingFile" short-circuit="true">
            <message>notExistingFile.exist</message>
        </field-validator>
    </field>

    <field name="existingDirectory">
        <field-validator type="requiredFile" short-circuit="true">
            <message>existingDirectory.required</message>
        </field-validator>

        <field-validator type="existingDirectory" short-circuit="true">
            <message>existingDirectory.not.exist</message>
        </field-validator>
    </field>

    <field name="notExistingDirectory">
        <field-validator type="requiredFile" short-circuit="true">
            <message>notExistingDirectory.required</message>
        </field-validator>

        <field-validator type="notExistingDirectory" short-circuit="true">
            <message>notExistingDirectory.exist</message>
        </field-validator>
    </field>

    <field name="entries">
        
        <field-validator type="collectionUniqueKey">
            <param name="keys">intValue</param>
            <message>collectionUniqueKey.one.failed</message>
        </field-validator>
        <field-validator type="collectionUniqueKey">
            <param name="keys">stringValue</param>
            <message>collectionUniqueKey.two.failed</message>
        </field-validator>
        <field-validator type="collectionUniqueKey">
            <param name="keys">intValue,stringValue</param>
            <message>collectionUniqueKey.three.failed</message>
        </field-validator>
        <field-validator type="collectionUniqueKey">
            <param name="keys">intValue,stringValue,stringValue2</param>
            <message>collectionUniqueKey.four.failed</message>
        </field-validator>
        <field-validator type="collectionUniqueKey">
            <param name="keys">stringValue</param>
            <param name="againstProperty">entry</param>
            <message>collectionUniqueKey.five.failed</message>
        </field-validator>

        <field-validator type="collectionFieldExpression">
            <param name="mode">AT_LEAST_ONE</param>
            <param name="expression"><![CDATA[ intValue == 0 && stringValue == "stringValue" ]]>
            </param>
            <message>collectionFieldExpression.atLeastOne</message>
        </field-validator>
        <field-validator type="collectionFieldExpression">
            <param name="mode">EXACTLY_ONE</param>
            <param name="expression"><![CDATA[ intValue == 0 && stringValue == "stringValue" ]]>
            </param>
            <message>collectionFieldExpression.exactlyOne</message>
        </field-validator>
        <field-validator type="collectionFieldExpression">
            <param name="mode">ALL</param>
            <param name="expression"><![CDATA[ intValue == 0 && stringValue == "stringValue" ]]>
            </param>
            <message>collectionFieldExpression.all</message>
        </field-validator>
        <field-validator type="collectionFieldExpression">
            <param name="mode">NONE</param>
            <param name="expression"><![CDATA[ intValue == 0 && stringValue == "stringValue" ]]>
            </param>
            <message>collectionFieldExpression.none</message>
        </field-validator>

        <!-- useContext -->
        <field-validator type="collectionFieldExpression">
            <param name="mode">AT_LEAST_ONE</param>
            <param name="useSensitiveContext">true</param>
            <param name="expression"><![CDATA[ size > 1 && previous != null && previous.intValue < current.intValue]]></param>
            <message>collectionFieldExpression.atLeastOne.useSensitiveContext</message>
        </field-validator>
        <field-validator type="collectionFieldExpression">
            <param name="mode">EXACTLY_ONE</param>
            <param name="useSensitiveContext">true</param>
            <param name="expression"><![CDATA[ size > 1 && previous != null && ( previous.intValue == 2 + current.intValue || current.intValue == 2 + previous.intValue) ]]></param>
            <message>collectionFieldExpression.exactlyOne.useSensitiveContext</message>
        </field-validator>
        <field-validator type="collectionFieldExpression">
            <param name="mode">ALL</param>
            <param name="useSensitiveContext">true</param>
            <param name="expression"><![CDATA[ size > 1 && (previous == null || previous.intValue < current.intValue)]]></param>
            <message>collectionFieldExpression.all.useSensitiveContext</message>
        </field-validator>
        <field-validator type="collectionFieldExpression">
            <param name="mode">NONE</param>
            <param name="useSensitiveContext">true</param>            
            <param name="expression"><![CDATA[ size > 1 && previous != null && ( current.intValue == 2 + previous.intValue)]]></param>
            <message>collectionFieldExpression.none.useSensitiveContext</message>
        </field-validator>

        <!-- useFirst -->        
        <field-validator type="collectionFieldExpression">
            <param name="mode">ALL</param>
            <param name="useSensitiveContext">true</param>
            <param name="expressionForFirst"><![CDATA[ current.intValue == 0]]></param>
            <param name="expression"><![CDATA[ previous == null || previous.intValue < current.intValue]]></param>
            <message>collectionFieldExpression.all.useFirst</message>
        </field-validator>

        <!-- useLast -->
        <field-validator type="collectionFieldExpression">
            <param name="mode">ALL</param>
            <param name="useSensitiveContext">true</param>
            <param name="expressionForLast"><![CDATA[ current.intValue > 0]]></param>
            <param name="expression"><![CDATA[ previous == null || previous.intValue < current.intValue]]></param>
            <message>collectionFieldExpression.all.useLast</message>
        </field-validator>

        <!-- useFirstAndLast -->
        <field-validator type="collectionFieldExpression">
            <param name="mode">ALL</param>
            <param name="useSensitiveContext">true</param>
            <param name="expressionForFirst"><![CDATA[ current.intValue == 0]]></param>
            <param name="expressionForLast"><![CDATA[ current.intValue > 0]]></param>
            <param name="expression"><![CDATA[ previous == null || previous.intValue < current.intValue]]></param>
            <message>collectionFieldExpression.all.useFirstAndLast</message>
        </field-validator>

    </field>

</validators>
