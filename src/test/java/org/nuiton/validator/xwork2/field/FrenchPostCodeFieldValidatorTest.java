/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Contact;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class FrenchPostCodeFieldValidatorTest extends AbstractFieldValidatorTest<Contact> {

    public FrenchPostCodeFieldValidatorTest() {
        super(Contact.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getPostCode());

        // Valid postCode
        bean.setPostCode("44230");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           false);

        // Valid postCode for Corsica
        bean.setEmail("2A220");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           false);

        // Valid postCode for DOM
        bean.setEmail("97120");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           false);

        // Valid postCode for TOM
        bean.setPostCode("98120");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           false);

        // Too long postCode
        bean.setPostCode("442300");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           true);

        // Too short postCode
        bean.setPostCode("4423");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           true);

        // postCode cannot start with 99
        bean.setPostCode("99230");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           true);

        // postCode can be empty (use a requiredstring validator for this check)
        bean.setPostCode("");
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           false);

        // postCode can be null (use a requiredstring validator for this check)
        bean.setPostCode(null);
        assertFieldInError(Contact.PROPERTY_POSTCODE, "contact.postCode.format",
                           false);


    }
}
