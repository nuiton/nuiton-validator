/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Contact;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class FrenchPhoneNumberFieldValidatorTest extends AbstractFieldValidatorTest<Contact> {

    public FrenchPhoneNumberFieldValidatorTest() {
        super(Contact.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getPhone());

        // Valid phone Number
        bean.setPhone("0000000000");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           false);

        // Valid phone Number
        bean.setPhone("00 00 00 00 00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           false);

        // Valid phone Number
        bean.setPhone("00-00-00-00-00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           false);

        // Valid phone Number
        bean.setPhone("00.00.00.00.00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           false);

        // Too long phone Number
        bean.setPhone("00 00 00 00 00 00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too long phone Number
        bean.setPhone("00-00-00-00-00-00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too long phone Number
        bean.setPhone("000000000000");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too long phone Number
        bean.setPhone("00.00.00.00.00.00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too short phone Number
        bean.setPhone("00.00.00.00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too short phone Number
        bean.setPhone("00000000");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too short phone Number
        bean.setPhone("00-00-00-00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Too short phone Number
        bean.setPhone("00 00 00 00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Invalid character in phone Number
        bean.setPhone("00 00 ab 00 00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Invalid character in phone Number
        bean.setPhone("0000ab0000");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Invalid character in phone Number
        bean.setPhone("00.00.ab.00.00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);

        // Invalid character in phone Number
        bean.setPhone("00-00-ab-00-00");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           true);
        //Use required string validator for this case
        bean.setPhone("");
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           false);
        //Use required validator for this case
        bean.setPhone(null);
        assertFieldInError(Contact.PROPERTY_PHONE, "contact.phoneNumber.format",
                           false);

    }
}
