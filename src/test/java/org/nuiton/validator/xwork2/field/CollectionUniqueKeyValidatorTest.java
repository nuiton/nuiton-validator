/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.xwork2.field.ValidatorBean.ValidatorBeanEntry;

import java.util.Arrays;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class CollectionUniqueKeyValidatorTest extends AbstractValidatorBeanFieldValidatorTest {

    static protected ValidatorBeanEntry beanEntry = new ValidatorBeanEntry(0, "stringValue");

    static protected ValidatorBeanEntry beanEntry2 = new ValidatorBeanEntry(0, "fake");

    static protected ValidatorBeanEntry beanEntry3 = new ValidatorBeanEntry(0, "stringValue", "stringValue2");

    @Test
    @Override
    public void testValidator() throws Exception {
        assertNull(bean.getEntries());

        // no entry
        assertFieldInError("entries", "collectionUniqueKey.one.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.five.failed", false);

        // add a entry
        bean.setEntries(Arrays.asList(beanEntry));

        assertFieldInError("entries", "collectionUniqueKey.one.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.five.failed", false);

        // add violating property
        bean.setEntry(beanEntry3);
        assertFieldInError("entries", "collectionUniqueKey.one.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.five.failed", true);


        // two entries with same key
        bean.setEntries(Arrays.asList(beanEntry, beanEntry));

        assertFieldInError("entries", "collectionUniqueKey.one.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", true);

        // add a entry
        bean.setEntries(Arrays.asList(beanEntry2));

        assertFieldInError("entries", "collectionUniqueKey.one.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", false);

        // add two entries (will violated unique key on intValue)
        bean.setEntries(Arrays.asList(beanEntry2, beanEntry));

        assertFieldInError("entries", "collectionUniqueKey.one.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", false);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", false);


        // two entries with same key (except validator four)
        bean.setEntries(Arrays.asList(beanEntry, beanEntry3));
        assertFieldInError("entries", "collectionUniqueKey.one.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", false);

        beanEntry.setStringValue2("stringValue2");
        // two entries with same key
        bean.setEntries(Arrays.asList(beanEntry, beanEntry3));
        assertFieldInError("entries", "collectionUniqueKey.one.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.two.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.three.failed", true);
        assertFieldInError("entries", "collectionUniqueKey.four.failed", true);


    }
}
