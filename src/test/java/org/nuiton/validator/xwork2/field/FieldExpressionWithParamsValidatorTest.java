/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FieldExpressionWithParamsValidatorTest extends AbstractFieldValidatorTest<FieldExpressionBean> {

    public static final String MESSAGE = "expression.too.big##100";

    public static final String MESSAGE2 = "expression.too.big##100##2000";

    public FieldExpressionWithParamsValidatorTest() {
        super(FieldExpressionBean.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        testBooleanType();
        testShortType();
        testIntType();
        testLongType();
        testDoubleType();
        testStringType();


    }

    protected void testBooleanType() {

        assertEquals(false, bean.isBooleanValue());
        assertFieldInError("booleanValue", "expression.boolean.not.equals##true", true);
        assertFieldInError("booleanValue", "expression.boolean.not.equals##false", false);

        bean.setBooleanValue(true);
        assertFieldInError("booleanValue", "expression.boolean.not.equals##true", false);
        assertFieldInError("booleanValue", "expression.boolean.not.equals##false", true);
    }

    protected void testShortType() {
        assertEquals(0, bean.getShortValue());
        assertFieldInError("shortValue", MESSAGE, false);
        assertFieldInError("shortValue", MESSAGE2, false);
        bean.setShortValue((short) 10);
        assertFieldInError("shortValue", MESSAGE, false);
        assertFieldInError("shortValue", MESSAGE2, false);
        bean.setShortValue((short) 1000);
        assertFieldInError("shortValue", MESSAGE, true);
        assertFieldInError("shortValue", MESSAGE2, false);
        bean.setShortValue((short) 3000);
        assertFieldInError("shortValue", MESSAGE, true);
        assertFieldInError("shortValue", MESSAGE2, true);
    }

    protected void testIntType() {
        assertEquals(0, bean.getIntValue());
        assertFieldInError("intValue", MESSAGE, false);
        assertFieldInError("intValue", MESSAGE2, false);
        bean.setIntValue(10);
        assertFieldInError("intValue", MESSAGE, false);
        assertFieldInError("intValue", MESSAGE2, false);
        bean.setIntValue(1000);
        assertFieldInError("intValue", MESSAGE, true);
        assertFieldInError("intValue", MESSAGE2, false);
        bean.setIntValue(3000);
        assertFieldInError("intValue", MESSAGE, true);
        assertFieldInError("intValue", MESSAGE2, true);
    }

    protected void testLongType() {
        assertEquals(0, bean.getLongValue());
        assertFieldInError("longValue", MESSAGE, false);
        assertFieldInError("longValue", MESSAGE2, false);
        bean.setLongValue(10);
        assertFieldInError("longValue", MESSAGE, false);
        assertFieldInError("longValue", MESSAGE2, false);
        bean.setLongValue(1000);
        assertFieldInError("longValue", MESSAGE, true);
        assertFieldInError("longValue", MESSAGE2, false);
        bean.setLongValue(3000);
        assertFieldInError("longValue", MESSAGE, true);
        assertFieldInError("longValue", MESSAGE2, true);
    }

    protected void testDoubleType() {
        assertEquals(0.0, bean.getDoubleValue(), 0);
        assertFieldInError("doubleValue", MESSAGE + ".0", false);
        assertFieldInError("doubleValue", "expression.too.big##100.0##2000.0", false);
        bean.setDoubleValue(10);
        assertFieldInError("doubleValue", MESSAGE + ".0", false);
        assertFieldInError("doubleValue", "expression.too.big##100.0##2000.0", false);
        bean.setDoubleValue(1000);
        assertFieldInError("doubleValue", MESSAGE + ".0", true);
        assertFieldInError("doubleValue", "expression.too.big##100.0##2000.0", false);
        bean.setDoubleValue(3000);
        assertFieldInError("doubleValue", MESSAGE + ".0", true);
        assertFieldInError("doubleValue", "expression.too.big##100.0##2000.0", true);
    }

    protected void testStringType() {
        assertEquals(null, bean.getStringValue());

        assertFieldInError("stringValue", "expression.stringNotValue##1000", true);
        assertFieldInError("stringValue", "expression.stringNotValue##1000##3000", true);
        bean.setStringValue("100");

        assertFieldInError("stringValue", "expression.stringNotValue##1000", true);
        assertFieldInError("stringValue", "expression.stringNotValue##1000##3000", true);

        bean.setStringValue("1000");
        assertFieldInError("stringValue", "expression.stringNotValue##1000", false);
        assertFieldInError("stringValue", "expression.stringNotValue##1000##3000", false);

        bean.setStringValue("3000");
        assertFieldInError("stringValue", "expression.stringNotValue##1000", true);
        assertFieldInError("stringValue", "expression.stringNotValue##1000##3000", false);
    }
}
