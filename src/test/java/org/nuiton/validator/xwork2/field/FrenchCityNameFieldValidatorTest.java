/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Contact;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class FrenchCityNameFieldValidatorTest extends AbstractFieldValidatorTest<Contact> {

    public FrenchCityNameFieldValidatorTest() {
        super(Contact.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getCity());

        // Valid city
        bean.setCity("Nantes");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Valid city
        bean.setCity("Couëron");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Valid city
        bean.setCity("Saint Sébastien Sur Loire");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Valid city
        bean.setCity("St Sebastien sur Loire");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Valid city
        bean.setCity("St-Sebastien-sur-Loire");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Valid city
        bean.setCity("Y");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Valid city
        bean.setCity("Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Not Valid city name
        bean.setCity("2Ville");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           true);

        // Not Valid city name
        bean.setCity("Ville2Merde");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           true);

        // Not Valid city name
        bean.setCity("Ville 2 Merde");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           true);

        // Use requiredstring validator for this case
        bean.setCity("");
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

        // Use required validator for this case
        bean.setCity(null);
        assertFieldInError(Contact.PROPERTY_CITY, "contact.city.format",
                           false);

    }
}
