/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class NotExistingDirectoryFieldValidatorTest extends AbstractValidatorBeanFieldValidatorTest {

    @Test
    @Override
    public void testValidator() throws Exception {
        assertNull(bean.getNotExistingDirectory());
        assertFieldInError("notExistingDirectory", "notExistingDirectory.required", true);

        bean.setNotExistingDirectory(new File(""));
        assertFieldInError("notExistingDirectory", "notExistingDirectory.required", true);

        // existing directory
        bean.setNotExistingDirectory(basedir);
        assertFieldInError("notExistingDirectory", "notExistingDirectory.required", false);
        assertFieldInError("notExistingDirectory", "notExistingDirectory.exist", true);

        // existing file
        bean.setNotExistingDirectory(new File(basedir, "pom.xml"));
        assertFieldInError("notExistingDirectory", "notExistingDirectory.required", false);
        assertFieldInError("notExistingDirectory", "notExistingDirectory.exist", true);

        // none existing directory
        bean.setNotExistingDirectory(new File(basedir, "pom.xml-" + System.currentTimeMillis()));
        assertFieldInError("notExistingDirectory", "notEexistingFile.required", false);
        assertFieldInError("notExistingDirectory", "notExistingDirectory.exist", false);
    }

}
