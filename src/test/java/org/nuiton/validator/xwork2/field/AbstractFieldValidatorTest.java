/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorResult;

import java.io.File;
import java.util.List;

/**
 * Abstract class to test a specific validator.
 *
 * To implements a test on a new validator, just extends this class
 * and implements the method {@link #testValidator()}.
 *
 * @param <B> the type of bean to validate.
 * @author Tony Chemit - chemit@codelutin.com
 */
public abstract class AbstractFieldValidatorTest<B> extends Assert {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(AbstractFieldValidatorTest.class);

    protected static NuitonValidator<?> cacheValidator;

    protected static File basedir;

    protected final Class<B> type;

    protected NuitonValidator<B> validator;

    protected B bean;

    public AbstractFieldValidatorTest(Class<B> type) {
        this.type = type;
    }

    /**
     * the method to test the given validator on the given bean.
     *
     * When coming here a validator and bean were instanciated and the bean was
     * setted into validator via setBean method.
     *
     * @throws Exception if any error ?
     */
    public abstract void testValidator() throws Exception;

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() throws Exception {
        log.debug("start test " + getClass().getSimpleName());
        bean = type.newInstance();
        if (cacheValidator == null) {
            validator = NuitonValidatorFactory.newValidator(type);
            cacheValidator = validator;
        } else {
            validator = (NuitonValidator<B>) cacheValidator;
        }
    }

    @After
    @SuppressWarnings("unchecked")
    public void tearDown() {
    }

    @AfterClass
    public static void afterclass() throws Exception {
        cacheValidator = null;
    }

    @BeforeClass
    public static void initValidator() throws Exception {

        String b = System.getenv("basedir");
        if (b == null) {
            b = new File("").getAbsolutePath();
        }
        basedir = new File(b);
    }

    @SuppressWarnings("unchecked")
    protected void assertFieldInError(String fieldName, String error, boolean required) {

        NuitonValidatorResult result = validator.validate(bean);

        List<String> errorMessages = result.getErrorMessages(fieldName);

        boolean errorFound = errorMessages.contains(error);

        if (required) {

            // must have this error

            assertTrue("error " + error + " should not exist but was found.", errorFound);
        } else {

            // must not have this error
            assertFalse("error " + error + " should exist but was not found.", errorFound);
        }
    }
}
