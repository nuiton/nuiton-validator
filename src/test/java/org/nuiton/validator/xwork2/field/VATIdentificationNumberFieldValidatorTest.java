/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Company;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class VATIdentificationNumberFieldValidatorTest extends AbstractFieldValidatorTest<Company> {

    public VATIdentificationNumberFieldValidatorTest() {
        super(Company.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getVat());

        // Valid vat
        bean.setVat("FR57442116703");
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           false);

        // Not valid vat
        bean.setVat("FR 57 44211670332");
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           true);

        // Not valid vat
        bean.setVat("442116703000389");
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           true);

        // Valid vat, with spaces in it
        bean.setVat("FR 57 442116703");
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           false);

        // Real vat, with spaces in it
        bean.setVat("FR 39 535 198 188");
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           false);

        // Use required string validator for that
        bean.setVat("");
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           false);

        // Use required validator for that
        bean.setVat(null);
        assertFieldInError(Company.PROPERTY_VAT_NUMBER, "company.vat.format",
                           false);
    }
}
