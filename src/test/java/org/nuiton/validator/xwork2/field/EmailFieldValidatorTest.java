/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Contact;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class EmailFieldValidatorTest extends AbstractFieldValidatorTest<Contact> {

    public EmailFieldValidatorTest() {
        super(Contact.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getEmail());

        // Valid email
        bean.setEmail("toto@toto.com");
        assertFieldInError(Contact.PROPERTY_EMAIL, "contact.email.format", false);

        // Valid email with +
        bean.setEmail("toto+validation@toto.com");
        assertFieldInError(Contact.PROPERTY_EMAIL, "contact.email.format", false);

        // Not valid email
        bean.setEmail("toto");
        assertFieldInError(Contact.PROPERTY_EMAIL, "contact.email.format", true);

        // use requiredstring for that case
        bean.setEmail("");
        assertFieldInError(Contact.PROPERTY_EMAIL, "contact.email.format",
                           false);

        // use required for that case
        bean.setEmail(null);
        assertFieldInError(Contact.PROPERTY_EMAIL, "contact.email.format",
                           false);

    }
}
