/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.xwork2.field.ValidatorBean.ValidatorBeanEntry;

import java.util.Arrays;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class CollectionFieldExpressionValidatorTest extends AbstractValidatorBeanFieldValidatorTest {

    protected static final String PROPERTY = "entries";

    static protected ValidatorBeanEntry beanEntry0 = new ValidatorBeanEntry(0, "stringValue");

    static protected ValidatorBeanEntry beanEntry0Bis = new ValidatorBeanEntry(0, "fake");

    static protected ValidatorBeanEntry beanEntry1 = new ValidatorBeanEntry(1, "fake");

    static protected ValidatorBeanEntry beanEntry3 = new ValidatorBeanEntry(3, "fake");

    static protected ValidatorBeanEntry beanEntry5 = new ValidatorBeanEntry(5, "fake");

    @Test
    @Override
    public void testValidator() throws Exception {
        assertNull(bean.getEntries());

        // no entry
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none", false);


        // add a matching etry
        bean.setEntries(Arrays.asList(beanEntry0));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none", true);

        // two matching etries
        bean.setEntries(Arrays.asList(beanEntry0, beanEntry0));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none", true);

        // add a none matching etry
        bean.setEntries(Arrays.asList(beanEntry0Bis));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none", false);

        // add a none matching etry and a matching entry
        bean.setEntries(Arrays.asList(beanEntry0Bis, beanEntry0));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none", true);
    }

    @Test
    public void testValidatorWithContext() throws Exception {
        assertNull(bean.getEntries());

        // no entry
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);

        // add a matching etry
        bean.setEntries(Arrays.asList(beanEntry0));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);

        // add a none matching etry
        bean.setEntries(Arrays.asList(beanEntry0Bis));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);

        // add a none matching etry and a matching entry
        bean.setEntries(Arrays.asList(beanEntry0Bis, beanEntry0));

        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1));
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);

        bean.setEntries(Arrays.asList(beanEntry1, beanEntry0));
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3));
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3, beanEntry5));
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry3, beanEntry1));
        assertFieldInError(PROPERTY, "collectionFieldExpression.atLeastOne.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.all.useSensitiveContext", true);
        assertFieldInError(PROPERTY, "collectionFieldExpression.exactlyOne.useSensitiveContext", false);
        assertFieldInError(PROPERTY, "collectionFieldExpression.none.useSensitiveContext", false);
    }

    @Test
    public void testValidatorWithContextAndFirst() throws Exception {
        assertNull(bean.getEntries());
        String message = "collectionFieldExpression.all.useFirst";

        // no entry
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0));
        assertFieldInError(PROPERTY, message, false);


        bean.setEntries(Arrays.asList(beanEntry0Bis));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry1));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry1, beanEntry0));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3, beanEntry5));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry3, beanEntry1));
        assertFieldInError(PROPERTY, message, true);
    }

    @Test
    public void testValidatorWithContextAndLast() throws Exception {
        assertNull(bean.getEntries());
        String message = "collectionFieldExpression.all.useLast";

        // no entry
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0));
        assertFieldInError(PROPERTY, message, true);


        bean.setEntries(Arrays.asList(beanEntry0Bis));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry1));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry1, beanEntry0));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry1, beanEntry3));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3, beanEntry5));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry3, beanEntry1));
        assertFieldInError(PROPERTY, message, true);
    }

    @Test
    public void testValidatorWithContextAndFirstAndLast() throws Exception {
        assertNull(bean.getEntries());

        String message = "collectionFieldExpression.all.useFirstAndLast";
        // no entry
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0));
        assertFieldInError(PROPERTY, message, true);


        bean.setEntries(Arrays.asList(beanEntry0Bis));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry1));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry1, beanEntry0));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry1, beanEntry3));
        assertFieldInError(PROPERTY, message, true);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry1, beanEntry3, beanEntry5));
        assertFieldInError(PROPERTY, message, false);

        bean.setEntries(Arrays.asList(beanEntry0, beanEntry3, beanEntry1));
        assertFieldInError(PROPERTY, message, true);
    }
}
