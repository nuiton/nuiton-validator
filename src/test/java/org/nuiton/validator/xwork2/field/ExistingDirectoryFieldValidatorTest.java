/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ExistingDirectoryFieldValidatorTest extends AbstractValidatorBeanFieldValidatorTest {

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getExistingDirectory());
        assertFieldInError("existingDirectory", "existingDirectory.required", true);

        bean.setExistingDirectory(new File(""));
        assertFieldInError("existingDirectory", "existingDirectory.required", true);

        // existing file
        bean.setExistingDirectory(new File(basedir, "pom.xml"));
        assertFieldInError("existingDirectory", "existingDirectory.required", false);
        assertFieldInError("existingDirectory", "existingDirectory.not.exist", true);

        // existing directory
        bean.setExistingDirectory(basedir);
        assertFieldInError("existingDirectory", "existingDirectory.required", false);
        assertFieldInError("existingDirectory", "existingDirectory.not.exist", false);

    }

}
