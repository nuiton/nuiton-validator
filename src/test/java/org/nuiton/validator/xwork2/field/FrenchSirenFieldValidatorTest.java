package org.nuiton.validator.xwork2.field;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Test;
import org.nuiton.validator.model.Company;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FrenchSirenFieldValidatorTest extends AbstractFieldValidatorTest<Company> {

    public FrenchSirenFieldValidatorTest() {
        super(Company.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getSiren());

        // Valid siren
        bean.setSiren("751700782");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                false);

        // Valid siren
        bean.setSiren("263903007");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                false);

        // Not Valid siren, because bad checksum
        bean.setSiren("263903000");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                true);

        // Not valid siren
        bean.setSiren("44211670");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                true);

        // Not valid siren
        bean.setSiren("442116703000389");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                true);

        // Not valid siren
        bean.setSiren("4421bf167");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                true);

        // Use requiredString for empty siren
        bean.setSiren("");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                false);

        // Use required for null siren
        bean.setSiren(null);
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                false);

        // Valid siren with spaces
        bean.setSiren("535 198 188 ");
        assertFieldInError(Company.PROPERTY_SIREN, "company.siren.format",
                false);

    }
}
