/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.Collection;

public class ValidatorBean {

    public static class ValidatorBeanEntry {

        protected int intValue;

        protected String stringValue;

        protected String stringValue2;

        public ValidatorBeanEntry(int intValue, String stringValue) {
            this.intValue = intValue;
            this.stringValue = stringValue;
        }

        public ValidatorBeanEntry(int intValue, String stringValue, String stringValue2) {
            this.intValue = intValue;
            this.stringValue = stringValue;
            this.stringValue2 = stringValue2;
        }

        public int getIntValue() {
            return intValue;
        }

        public void setIntValue(int intValue) {
            this.intValue = intValue;
        }

        public String getStringValue() {
            return stringValue;
        }

        public void setStringValue(String stringValue) {
            this.stringValue = stringValue;
        }

        public String getStringValue2() {
            return stringValue2;
        }

        public void setStringValue2(String stringValue2) {
            this.stringValue2 = stringValue2;
        }
    }

    protected File existingFile;

    protected File notExistingFile;

    protected File existingDirectory;

    protected File notExistingDirectory;

    protected Collection<ValidatorBeanEntry> entries;

    protected String stringValue;

    protected ValidatorBeanEntry entry;

    PropertyChangeSupport p;

    public ValidatorBean() {
        p = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        p.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        p.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        p.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        p.removePropertyChangeListener(propertyName, listener);
    }

    public String getStringValue() {
        return stringValue;
    }

    public File getExistingFile() {
        return existingFile;
    }

    public File getNotExistingFile() {
        return notExistingFile;
    }

    public File getExistingDirectory() {
        return existingDirectory;
    }

    public File getNotExistingDirectory() {
        return notExistingDirectory;
    }

    public ValidatorBeanEntry getEntry() {
        return entry;
    }

    public Collection<ValidatorBeanEntry> getEntries() {
        return entries;
    }

    public void setStringValue(String stringValue) {
        String old = this.stringValue;
        this.stringValue = stringValue;
        p.firePropertyChange("stringValue", old, existingFile);
    }

    public void setExistingFile(File existingFile) {
        File old = this.existingFile;
        this.existingFile = existingFile;
        p.firePropertyChange("existingFile", old, existingFile);
    }

    public void setNotExistingFile(File notExistingFile) {
        File old = this.notExistingFile;
        this.notExistingFile = notExistingFile;
        p.firePropertyChange("notExistingFile", old, notExistingFile);
    }

    public void setExistingDirectory(File existingDirectory) {
        File old = this.existingDirectory;
        this.existingDirectory = existingDirectory;
        p.firePropertyChange("existingDirectory", old, existingDirectory);
    }

    public void setNotExistingDirectory(File notExistingDirectory) {
        File old = this.notExistingDirectory;
        this.notExistingDirectory = notExistingDirectory;
        p.firePropertyChange("notExistingDirectory", old, notExistingDirectory);
    }

    public void setEntries(Collection<ValidatorBeanEntry> entries) {
        this.entries = entries;
        // set null oldValue to always fire event
        // otherwise it could been not sent...
        p.firePropertyChange("entries", null, entries);
    }

    public void setEntry(ValidatorBeanEntry entry) {
        this.entry = entry;
        p.firePropertyChange("entry", null, entry);
    }
}
