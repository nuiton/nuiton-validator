/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class NotExistingFileFieldValidatorTest extends AbstractValidatorBeanFieldValidatorTest {

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getNotExistingFile());
        assertFieldInError("notExistingFile", "notExistingFile.required", true);

        bean.setNotExistingFile(new File(""));
        assertFieldInError("notExistingFile", "notExistingFile.required", true);

        // existing directory
        bean.setNotExistingFile(basedir);
        assertFieldInError("notExistingFile", "notExistingFile.required", false);
        assertFieldInError("notExistingFile", "notExistingFile.exist", true);

        // existing file
        bean.setNotExistingFile(new File(basedir, "pom.xml"));
        assertFieldInError("notExistingFile", "notExistingFile.required", false);
        assertFieldInError("notExistingFile", "notExistingFile.exist", true);

        // none existing file
        bean.setNotExistingFile(new File(basedir, "pom.xml-" + System.currentTimeMillis()));
        assertFieldInError("notExistingFile", "notEexistingFile.required", false);
        assertFieldInError("notExistingFile", "notExistingFile.exist", false);


    }

}
