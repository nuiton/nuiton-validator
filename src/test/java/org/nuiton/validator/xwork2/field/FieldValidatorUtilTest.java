package org.nuiton.validator.xwork2.field;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FieldValidatorUtilTest extends Assert {

    @Test
    public void TestLuhnChecksum() throws Exception {
        // verification sur de vrai numero siret (ca doit passer :)
        assertTrue(FieldValidatorUtil.luhnChecksum("44211670300038"));
        assertTrue(FieldValidatorUtil.luhnChecksum("73282932000074"));
        assertTrue(FieldValidatorUtil.luhnChecksum("2A0002879"));

        // verification avec les memes, en ne modifiant que le dernier chiffre
        // ca doit failer
        assertFalse(FieldValidatorUtil.luhnChecksum("44211670300030"));
        assertFalse(FieldValidatorUtil.luhnChecksum("73282932000070"));
        assertFalse(FieldValidatorUtil.luhnChecksum("2A0002870"));
    }

}
