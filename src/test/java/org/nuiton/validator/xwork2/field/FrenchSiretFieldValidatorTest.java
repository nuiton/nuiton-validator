/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Company;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class FrenchSiretFieldValidatorTest extends AbstractFieldValidatorTest<Company> {

    public FrenchSiretFieldValidatorTest() {
        super(Company.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getSiret());

        // Valid siret
        bean.setSiret("44211670300038");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           false);

        // Valid siret
        bean.setSiret("73282932000074");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           false);

        // Not Valid siret, because bad checksum
        bean.setSiret("73282932000071");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           true);

        // Not valid siret
        bean.setSiret("4421167030003");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           true);

        // Not valid siret
        bean.setSiret("442116703000389");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           true);

        // Not valid siret
        bean.setSiret("4421bf1670300038");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           true);

        // Use requiredString for empty siret
        bean.setSiret("");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           false);

        // Use required for null siret
        bean.setSiret(null);
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           false);

        // Valid siret with spaces
        bean.setSiret("535 198 188 00018 ");
        assertFieldInError(Company.PROPERTY_SIRET, "company.siret.format",
                           false);

    }
}
