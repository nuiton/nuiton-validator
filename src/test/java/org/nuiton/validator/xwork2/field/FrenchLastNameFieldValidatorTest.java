/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import org.junit.Test;
import org.nuiton.validator.model.Contact;

/**
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class FrenchLastNameFieldValidatorTest extends AbstractFieldValidatorTest<Contact> {

    public FrenchLastNameFieldValidatorTest() {
        super(Contact.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {

        assertNull(bean.getCity());

        // Valid name
        bean.setName("Couteau");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           false);

        // Valid name
        bean.setName("Le Ny");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           false);

        // Valid name
        bean.setName("Clémenceau");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           false);

        // Valid name
        bean.setName("Couteau--Viney");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           false);

        // Not valid name
        bean.setName("2Pac");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           true);

        // Not valid name
        bean.setName("Nom2Merde");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           true);

        // Use requiredstring validator for that case
        bean.setName("");
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           false);

        // Use required validator for that case
        bean.setName(null);
        assertFieldInError(Contact.PROPERTY_LAST_NAME, "contact.name.format",
                           false);


    }
}
