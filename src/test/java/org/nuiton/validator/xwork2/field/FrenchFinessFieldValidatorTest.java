package org.nuiton.validator.xwork2.field;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Test;
import org.nuiton.validator.model.HealthEstablishment;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FrenchFinessFieldValidatorTest extends AbstractFieldValidatorTest<HealthEstablishment> {

    public FrenchFinessFieldValidatorTest() {
        super(HealthEstablishment.class);
    }

    @Test
    @Override
    public void testValidator() throws Exception {
        assertNull(bean.getFiness());

        // Valid Finess
        bean.setFiness("440012664");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           false);

        // Valid Finess
        bean.setFiness("310001177");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                false);

        // Valid Finess
        bean.setFiness("2A0002879");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                false);

        // Valid Finess
        bean.setFiness("2B0000236");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           false);

        // not Valid Finess
        bean.setFiness("2B01");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           true);

        // not Valid Finess
        bean.setFiness("4402345636");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           true);

        // not Valid Finess
        bean.setFiness("CC0123456");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           true);

        // not Valid Finess
        bean.setFiness("000123456");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           true);

        // not Valid Finess
        bean.setFiness("441B23456");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           true);

        // not Valid Finess
        bean.setFiness("4402345F6");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                           true);

        // not Valid Finess
        bean.setFiness("440012666");
        assertFieldInError(HealthEstablishment.PROPERTY_FINESS, "healthEstablishment.finess.format",
                true);

    }
}
