/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Tony Chemit - chemit@codelutin.com
 */
public class FieldExpressionBean {

    protected final PropertyChangeSupport p;

    protected boolean booleanValue;

    protected short shortValue;

    protected int intValue;

    protected long longValue;

    protected double doubleValue;

    protected String stringValue;

    public FieldExpressionBean() {
        p = new PropertyChangeSupport(this);
    }

    public boolean isBooleanValue() {
        return booleanValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public long getLongValue() {
        return longValue;
    }

    public short getShortValue() {
        return shortValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setBooleanValue(boolean newValue) {
        Object oldValue = booleanValue;
        booleanValue = newValue;
        firePropertyChange("booleanValue", oldValue, newValue);
    }

    public void setDoubleValue(double newValue) {
        Object oldValue = doubleValue;
        doubleValue = newValue;
        firePropertyChange("doubleValue", oldValue, newValue);
    }

    public void setIntValue(int newValue) {
        Object oldValue = stringValue;
        intValue = newValue;
        firePropertyChange("intValue", oldValue, newValue);
    }

    public void setLongValue(long newValue) {
        Object oldValue = longValue;
        longValue = newValue;
        firePropertyChange("longValue", oldValue, newValue);
    }

    public void setShortValue(short newValue) {
        Object oldValue = shortValue;
        shortValue = newValue;
        firePropertyChange("shortValue", oldValue, newValue);
    }

    public void setStringValue(String newValue) {
        Object oldValue = stringValue;
        stringValue = newValue;
        firePropertyChange("stringValue", oldValue, newValue);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        p.firePropertyChange(propertyName, oldValue, newValue);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        p.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        p.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        p.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        p.removePropertyChangeListener(propertyName, listener);
    }
}
