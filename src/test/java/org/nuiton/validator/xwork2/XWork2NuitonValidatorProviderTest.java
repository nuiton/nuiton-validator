/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.ValidatorTestHelper;
import org.nuiton.validator.model.Person;
import org.nuiton.validator.model.Pet;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.regex.Pattern;

/**
 * To test {@link XWork2NuitonValidatorProvider}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class XWork2NuitonValidatorProviderTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(XWork2NuitonValidatorProviderTest.class);

    protected XWork2NuitonValidatorProvider provider;

    @Before
    public void setUp() {
        provider = new XWork2NuitonValidatorProvider();
    }

    @Test
    public void testGetModel() throws Exception {

        NuitonValidatorModel<Person> model =
                provider.getModel(Person.class, null);

        Assert.assertNotNull(model);
        Assert.assertNull(model.getContext());
        Assert.assertEquals(Person.class, model.getType());
        Set<NuitonValidatorScope> scopes = new HashSet<NuitonValidatorScope>(
                Arrays.asList(NuitonValidatorScope.values()));
        Assert.assertEquals(scopes, model.getScopes());
    }

    @Test
    public void testNewValidator() throws Exception {


        NuitonValidatorModel<Person> model =
                provider.getModel(Person.class, null);

        NuitonValidator<Person> validator = provider.newValidator(model);

        Assert.assertNotNull(validator);
    }

    @Test
    public void testDetectValidators() {

        String context = "context";

        File basedir = ValidatorTestHelper.getBasedir();
        File testResourcesDir = new File(basedir, "src" + File.separator + "test" + File.separator + "resources");

        SortedSet<NuitonValidator<?>> result;
        NuitonValidator<?> validator;
        Iterator<NuitonValidator<?>> iterator;

        // test with all context and all scopes : two validators (Person + Pet + Pet (context))

        result = provider.detectValidators(testResourcesDir, null, null, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());


        iterator = result.iterator();
        validator = iterator.next();

        ValidatorTestHelper.assertValidatorModel(validator, null, Person.class, NuitonValidatorScope.values());
        ValidatorTestHelper.assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING);

        validator = iterator.next();
        ValidatorTestHelper.assertValidatorModel(validator, null, Pet.class, NuitonValidatorScope.values());
        ValidatorTestHelper.assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR);

        validator = iterator.next();
        ValidatorTestHelper.assertValidatorModel(validator, context, Pet.class, NuitonValidatorScope.values());
        ValidatorTestHelper.assertValidatorEffectiveScopes(validator, NuitonValidatorScope.INFO);

        // test with no context and only scope warning : one validator (Person)

        result = provider.detectValidators(testResourcesDir, null, new NuitonValidatorScope[]{NuitonValidatorScope.WARNING}, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        iterator = result.iterator();

        validator = iterator.next();
        ValidatorTestHelper.assertValidatorModel(validator, null, Person.class, NuitonValidatorScope.WARNING);
        ValidatorTestHelper.assertValidatorEffectiveScopes(validator, NuitonValidatorScope.WARNING);

        // test with context 'context' and all scopes  : one validator (Pet)

        result = provider.detectValidators(testResourcesDir, Pattern.compile(context), null, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        iterator = result.iterator();

        validator = iterator.next();
        ValidatorTestHelper.assertValidatorModel(validator, context, Pet.class, NuitonValidatorScope.values());
        ValidatorTestHelper.assertValidatorEffectiveScopes(validator, NuitonValidatorScope.INFO);

        // test with no context and only scope fatal : no validator
        result = provider.detectValidators(testResourcesDir, null,
                                           new NuitonValidatorScope[]{NuitonValidatorScope.FATAL}, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());

        // test with specific context fake and all scopes : no validator

        result = provider.detectValidators(testResourcesDir, Pattern.compile(".*-fake"),
                                           null, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

}
