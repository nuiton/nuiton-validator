/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.ValidatorTestHelper;
import org.nuiton.validator.model.Family;
import org.nuiton.validator.model.Person;

/**
 * To test {@link XWork2NuitonValidator}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class XWork2NuitonValidatorTest {

    @Test
    public void testNewValidator() throws Exception {

        XWork2NuitonValidatorProvider provider =
                new XWork2NuitonValidatorProvider();


        NuitonValidatorModel<Person> model =
                provider.getModel(Person.class, null);

        NuitonValidator<Person> validator = provider.newValidator(model);

        Assert.assertNotNull(validator);

        ValidatorTestHelper.testPerson(validator);

    }

    @Test
    public void testNewValidator2() throws Exception {

        XWork2NuitonValidatorProvider provider =
                new XWork2NuitonValidatorProvider();

        NuitonValidatorModel<Family> model =
                provider.getModel(Family.class, null);

        NuitonValidator<Family> validator = provider.newValidator(model);

        Assert.assertNotNull(validator);

        Family f = new Family();
        Person father = new Person();
        Person mother = new Person();
        f.setMember(Sets.newHashSet(father, mother));

        NuitonValidatorResult validate = validator.validate(f);

        Assert.assertFalse(validate.isValid());

        father.setFirstname("john");
        father.setName("smith");
        mother.setFirstname("jim"); // since 18 mai 2013 possible in France!
        mother.setName("smith");

        validate = validator.validate(f);

        Assert.assertTrue(validate.isValid());
    }
}
