/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.model;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.validator.AbstractValidatorDetectorTest;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.io.File;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.regex.Pattern;

/**
 * Tests the validators defined for the test model.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class ModelValidatorDetectorTestImpl extends AbstractValidatorDetectorTest {

    public static final String CONTEXT = "context";

    public ModelValidatorDetectorTestImpl() {
        super(XWork2NuitonValidatorProvider.PROVIDER_NAME);
    }

    @Override
    protected File getRootDirectory(File basedir) {
        File testResourcesDir = new File(basedir, "src" + File.separator + "test" + File.separator + "resources");
        return testResourcesDir;
    }

    @Test
    public void detectAllValidators() {
        SortedSet<NuitonValidator<?>> result;
        NuitonValidator<?> validator;
        Iterator<NuitonValidator<?>> iterator;

        // test with all context and all scopes : 3 validators (Person + Pet + Pet (context))

        result = detectValidators(Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());

        iterator = result.iterator();
        validator = iterator.next();

        assertValidatorModel(validator, null, Person.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR, NuitonValidatorScope.WARNING);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.ERROR, Person.PROPERTY_NAME, Person.PROPERTY_FIRSTNAME);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.WARNING, Person.PROPERTY_PET);

        validator = iterator.next();
        assertValidatorModel(validator, null, Pet.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.ERROR);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.ERROR, Pet.PROPERTY_NAME);

        validator = iterator.next();
        assertValidatorModel(validator, CONTEXT, Pet.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.INFO);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.INFO, Pet.PROPERTY_NAME);
    }

    @Test
    public void detectValidatorsWithFilteredScopes() {

        SortedSet<NuitonValidator<?>> result;
        NuitonValidator<?> validator;
        Iterator<NuitonValidator<?>> iterator;

        // test with no context and only scope warning : one validator (Person)

        result = detectValidators(null, new NuitonValidatorScope[]{NuitonValidatorScope.WARNING}, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        iterator = result.iterator();

        validator = iterator.next();
        assertValidatorModel(validator, null, Person.class, NuitonValidatorScope.WARNING);
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.WARNING);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.WARNING, Person.PROPERTY_PET);

        // test with no context and only fatal scope : no validator

        result = detectValidators(null,
                                  new NuitonValidatorScope[]{NuitonValidatorScope.FATAL}, Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void detectValidatorsWithFilteredContext() {

        SortedSet<NuitonValidator<?>> result;
        NuitonValidator<?> validator;
        Iterator<NuitonValidator<?>> iterator;


        // test with context 'context' and all scopes  : one validator (Pet)

        result = detectValidators(Pattern.compile(CONTEXT), Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        iterator = result.iterator();

        validator = iterator.next();
        assertValidatorModel(validator, CONTEXT, Pet.class, NuitonValidatorScope.values());
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.INFO);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.INFO, Pet.PROPERTY_NAME);


        // test with specific context fake and all scopes : no validator

        result = detectValidators(Pattern.compile(".*-fake"),
                                  Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void detectValidatorsWithFilteredContextAndFilteredScope() {

        SortedSet<NuitonValidator<?>> result;
        NuitonValidator<?> validator;
        Iterator<NuitonValidator<?>> iterator;

        // test with context 'context' and info-fatal scopes : one validator (Pet)

        result = detectValidators(Pattern.compile(CONTEXT),
                                  new NuitonValidatorScope[]{NuitonValidatorScope.INFO, NuitonValidatorScope.FATAL},
                                  Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());

        iterator = result.iterator();

        validator = iterator.next();
        assertValidatorModel(validator, CONTEXT, Pet.class, NuitonValidatorScope.FATAL, NuitonValidatorScope.INFO);
        assertValidatorEffectiveScopes(validator, NuitonValidatorScope.INFO);
        assertValidatorEffectiveFields(validator, NuitonValidatorScope.INFO, Pet.PROPERTY_NAME);

        // test with specific context fake and fatal scope : no validator

        result = detectValidators(Pattern.compile(".*-fake"),
                                  new NuitonValidatorScope[]{NuitonValidatorScope.FATAL},
                                  Person.class, Pet.class);

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isEmpty());
    }


}
