/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class Pet {

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_TYPE = "type";

    public static final String PROPERTY_PERSON = "person";

    public static final String PROPERTY_RACE = "race";

    protected String name;

    protected String type;

    protected Person person;

    protected Race race;


    public void setName(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getType() {
        return type;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


    public Person getPerson() {
        return person;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Race getRace() {
        return race;
    }

    @Override
    public String toString() {
        String result = new ToStringBuilder(this).
                append(PROPERTY_NAME, this.name).
                append(PROPERTY_TYPE, this.type).
                append(PROPERTY_RACE, this.race).
                toString();
        return result;
    }

}
