/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.Collection;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class Person {

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_FIRSTNAME = "firstname";

    public static final String PROPERTY_PET = "pet";

    protected String name;

    protected String firstname;

    protected Collection<Pet> pet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Collection<Pet> getPet() {
        return pet;
    }

    public void setPet(Collection<Pet> pet) {
        this.pet = pet;
    }

    public void addPet(Pet pet) {
        if (this.pet == null) {
            this.pet = new ArrayList<Pet>();
        }

        pet.setPerson(this);

        this.pet.add(pet);
    }

    @Override
    public String toString() {
        String result = new ToStringBuilder(this).
                append(PROPERTY_NAME, name).
                append(PROPERTY_FIRSTNAME, firstname).
                toString();
        return result;
    }
}
