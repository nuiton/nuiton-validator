/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import java.io.File;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.regex.Pattern;

/**
 * Abstract test to detects and test your validators.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public abstract class AbstractValidatorDetectorTest {

    protected final String providerName;

    protected File rootDirectory;

    public AbstractValidatorDetectorTest(String providerName) {
        this.providerName = providerName;
    }

    protected NuitonValidatorProvider provider;

    protected abstract File getRootDirectory(File basedir);

    @Before
    public void setUp() throws Exception {
        provider = NuitonValidatorFactory.getProvider(providerName);
        rootDirectory = getRootDirectory(ValidatorTestHelper.getBasedir());
    }

    @After
    public void tearDown() throws Exception {
        provider = null;
    }

    protected SortedSet<NuitonValidator<?>> detectValidators(Class<?>... types) {
        return detectValidators(null, NuitonValidatorScope.values(), types);
    }

    protected SortedSet<NuitonValidator<?>> detectValidators(Pattern context, Class<?>... types) {
        return detectValidators(context, NuitonValidatorScope.values(), types);
    }

    protected SortedSet<NuitonValidator<?>> detectValidators(Pattern context, NuitonValidatorScope[] scopes, Class<?>... types) {
        SortedSet<NuitonValidator<?>> validators =
                provider.detectValidators(rootDirectory, context, scopes, types);
        return validators;
    }

    public void assertValidatorModel(NuitonValidator<?> validator,
                                     String expectedContext,
                                     Class<?> expectedType,
                                     NuitonValidatorScope... expectedScopes) {
        ValidatorTestHelper.assertValidatorModel(
                validator,
                expectedContext,
                expectedType,
                expectedScopes
        );
    }

    public void assertValidatorEffectiveScopes(NuitonValidator<?> validator,
                                               NuitonValidatorScope... expectedScopes) {
        ValidatorTestHelper.assertValidatorEffectiveScopes(validator,
                                                           expectedScopes
        );
    }

    public void assertValidatorEffectiveFields(NuitonValidator<?> validator,
                                               String... expectedFields) {

        ValidatorTestHelper.assertValidatorEffectiveFields(validator,
                                                           expectedFields);
    }

    public void assertValidatorEffectiveFields(NuitonValidator<?> validator,
                                               NuitonValidatorScope scope,
                                               String... expectedFields) {
        ValidatorTestHelper.assertValidatorEffectiveFields(validator,
                                                           scope,
                                                           expectedFields);
    }

    public void assertValidatorSetWithMultiContextName(SortedSet<NuitonValidator<?>> result,
                                                       Object... contextThenClass) {

        Assert.assertNotNull(result);
        Assert.assertEquals(contextThenClass.length / 2, result.size());

        Iterator<NuitonValidator<?>> itr = result.iterator();
        int index = 0;

        NuitonValidatorScope[] scopes = NuitonValidatorScope.values();

        while (itr.hasNext()) {
            NuitonValidator<?> next = itr.next();
            ValidatorTestHelper.assertValidatorModel(next,
                                                     (String) contextThenClass[2 * index],
                                                     (Class<?>) contextThenClass[2 * index + 1],
                                                     scopes);
            index++;
        }

    }

    public void assertValidatorSetWithSameContextName(SortedSet<NuitonValidator<?>> result,
                                                      String context,
                                                      Class<?>... contextThenClass) {

        Assert.assertNotNull(result);
        Assert.assertEquals(contextThenClass.length, result.size());

        Iterator<NuitonValidator<?>> itr = result.iterator();
        int index = 0;

        NuitonValidatorScope[] scopes = NuitonValidatorScope.values();

        while (itr.hasNext()) {
            NuitonValidator<?> next = itr.next();
            ValidatorTestHelper.assertValidatorModel(next,
                                                     context,
                                                     contextThenClass[index++],
                                                     scopes);
        }

    }


}
