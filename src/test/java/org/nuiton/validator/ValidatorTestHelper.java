/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import org.junit.Assert;
import org.nuiton.validator.model.Person;
import org.nuiton.validator.model.Pet;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Helper methods to test the validator api.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class ValidatorTestHelper {

    public static File getBasedir() {

        // Search basedir from maven environment
        String basedirPath = System.getenv("basedir");
        if (basedirPath == null) {

            // hope the tests are running from the root of the module :)
            basedirPath = new File("").getAbsolutePath();
        }
        return new File(basedirPath);
    }

    public static void assertValidatorModel(NuitonValidator<?> validator,
                                            String expectedContext,
                                            Class<?> expectedType,
                                            NuitonValidatorScope... expectedScopes) {
        Assert.assertNotNull(validator);
        NuitonValidatorModel<?> model = validator.getModel();
        Assert.assertNotNull(model);
        Assert.assertEquals(expectedContext, model.getContext());
        Assert.assertEquals(expectedType, model.getType());
        Set<NuitonValidatorScope> scopes = model.getScopes();
        for (NuitonValidatorScope expectedScope : expectedScopes) {
            Assert.assertTrue(scopes.contains(expectedScope));
        }
    }

    public static void assertValidatorEffectiveScopes(NuitonValidator<?> validator,
                                                      NuitonValidatorScope... expectedScopes) {
        Assert.assertNotNull(validator);
        Set<NuitonValidatorScope> effectiveScopes = validator.getEffectiveScopes();
        Assert.assertEquals(expectedScopes.length, effectiveScopes.size());
        for (NuitonValidatorScope expectedScope : expectedScopes) {
            Assert.assertTrue(effectiveScopes.contains(expectedScope));
        }
    }

    public static void assertValidatorEffectiveFields(NuitonValidator<?> validator,
                                                      String... expectedFields) {
        Assert.assertNotNull(validator);
        Set<String> effectiveFields = validator.getEffectiveFields();
        Assert.assertEquals(expectedFields.length, effectiveFields.size());
        for (String expectedField : expectedFields) {
            Assert.assertTrue(effectiveFields.contains(expectedField));
        }
    }

    public static void assertValidatorEffectiveFields(NuitonValidator<?> validator,
                                                      NuitonValidatorScope scope,
                                                      String... expectedFields) {
        Assert.assertNotNull(validator);
        Set<String> effectiveFields = validator.getEffectiveFields(scope);
        Assert.assertEquals(expectedFields.length, effectiveFields.size());
        for (String expectedField : expectedFields) {
            Assert.assertTrue(effectiveFields.contains(expectedField));
        }
    }

    public static void testPerson(NuitonValidator<Person> validator) {
        Assert.assertNotNull(validator);

        Person person = new Person();

        NuitonValidatorResult result;

        result = validator.validate(person);

        // two errors : no name, no firstname
        // one warning : no pet
        Assert.assertFalse(result.isValid());
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_FIRSTNAME, "person.firstname.required");
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_NAME, "person.name.required");
        assertFieldMessages(result, NuitonValidatorScope.WARNING, Person.PROPERTY_PET, "person.with.no.pet");

        person.setFirstname("Joe");
        result = validator.validate(person);

        // one error   : no name
        // one warning : no pet
        Assert.assertFalse(result.isValid());
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_FIRSTNAME);
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_NAME, "person.name.required");
        assertFieldMessages(result, NuitonValidatorScope.WARNING, Person.PROPERTY_PET, "person.with.no.pet");

        person.setName("Black");
        result = validator.validate(person);

        // no error
        // one warning : no pet
        Assert.assertTrue(result.isValid());
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_FIRSTNAME);
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_NAME);
        assertFieldMessages(result, NuitonValidatorScope.WARNING, Person.PROPERTY_PET, "person.with.no.pet");

        person.addPet(new Pet());
        result = validator.validate(person);

        // no error
        // no warning
        Assert.assertTrue(result.isValid());
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_FIRSTNAME);
        assertFieldMessages(result, NuitonValidatorScope.ERROR, Person.PROPERTY_NAME);
        assertFieldMessages(result, NuitonValidatorScope.WARNING, Person.PROPERTY_PET);

    }

    public static void assertFieldMessages(NuitonValidatorResult result,
                                           NuitonValidatorScope scope,
                                           String field,
                                           String... expectedMessages) {

        if (expectedMessages.length == 0) {

            // no messages
            boolean hasMessages = result.hasMessagesForScope(field, scope);
            Assert.assertFalse(hasMessages);

        } else {

            // with messages
            boolean hasMessages = result.hasMessagesForScope(field, scope);
            Assert.assertTrue(hasMessages);

            List<String> messages = result.getMessagesForScope(field, scope);
            Assert.assertNotNull(hasMessages);
            Assert.assertEquals(expectedMessages.length, messages.size());
            for (String expectedMessage : expectedMessages) {
                Assert.assertTrue(messages.contains(expectedMessage));
            }
        }
    }
}
