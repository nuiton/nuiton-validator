/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.bean.simple;

import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.SimpleBean;

import java.util.Arrays;
import java.util.List;

/**
 * To test the {@link SimpleBeanValidator}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public class SimpleBeanValidatorTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(SimpleBeanValidatorTest.class);

    protected SimpleBeanValidator<SimpleBean> validator;

    protected SimpleBean bean;

    ValidatorListenerImpl fatalListener;

    ValidatorListenerImpl errorListener;

    ValidatorListenerImpl warningListener;

    ValidatorListenerImpl infoListener;

    @Before
    public void setUp() {

        bean = new SimpleBean();
    }

    protected void prepareValidator(String context) {

        validator = SimpleBeanValidator.newValidator(SimpleBean.class, context);

        validator.addSimpleBeanValidatorListener(fatalListener = new ValidatorListenerImpl(NuitonValidatorScope.FATAL));
        validator.addSimpleBeanValidatorListener(errorListener = new ValidatorListenerImpl(NuitonValidatorScope.ERROR));
        validator.addSimpleBeanValidatorListener(warningListener = new ValidatorListenerImpl(NuitonValidatorScope.WARNING));
        validator.addSimpleBeanValidatorListener(infoListener = new ValidatorListenerImpl(NuitonValidatorScope.INFO));
    }

    @After
    public void tearDown() {
        bean = null;
        if (validator != null) {
            validator.setBean(null);
            validator = null;
        }
    }

    private static final String STRING_VALUE_FATAL = "stringValue.fatal";

    private static final String STRING_VALUE_ERROR = "stringValue.error";

    private static final String STRING_VALUE_WARNING = "stringValue.warning";

    private static final String INT_VALUE_FATAL = "intValue.fatal";

    private static final String INT_VALUE_ERROR = "intValue.error";

    private static final String INT_VALUE_INFO = "intValue.info";

    @Test(expected = IllegalStateException.class)
    public void testValidateWithBad() {

        // with marchepo context, there is a unknown field in scope error

        prepareValidator("marchepo");
    }

    @Test
    public void validate() {

        prepareValidator(null);

        assertMessages(fatalListener);
        assertMessages(errorListener);
        assertMessages(warningListener);
        assertMessages(infoListener);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        validator.setBean(bean);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        bean.setStringValue("one");

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, INT_VALUE_ERROR);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        bean.setStringValue("oneone");

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, INT_VALUE_ERROR);
        assertMessages(warningListener);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        bean.setIntValue(1);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener);
        assertMessages(warningListener);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        bean.setIntValue(10);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener);
        assertMessages(warningListener);
        assertMessages(infoListener);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        bean.setStringValue(null);
        bean.setIntValue(0);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);


        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        bean.setStringValue("5");
        bean.setIntValue(5);
        assertMessages(fatalListener);
        assertMessages(errorListener);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);
    }

    @Test
    public void convert() {

        prepareValidator(null);

        assertMessages(errorListener);
        assertMessages(warningListener);
        assertMessages(infoListener);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        validator.setBean(bean);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);


        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        Object value = validator.convert("intValue", "abc", Class.class);

        Assert.assertNull(value);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, STRING_VALUE_ERROR, "error.convertor.class");
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        bean.setStringValue("one");

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, "error.convertor.class");
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        value = validator.convert("intValue", "3", Integer.class);

        bean.setIntValue((Integer) value);

        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        bean.setIntValue(-1);
        assertMessages(fatalListener, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, INT_VALUE_ERROR);
        assertMessages(warningListener, STRING_VALUE_WARNING);
        assertMessages(infoListener, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
    }

    void assertMessages(ValidatorListenerImpl listener,
                        String... expected) {
        List<String> actual = listener.getMessages();
        Assert.assertEquals(" shoudl have " +
                            Arrays.toString(expected) + " but had " + actual,
                            expected.length, actual.size());
        for (String m : expected) {
            Assert.assertEquals("could not find " + m + " in " + actual,
                                true, actual.contains(m));
        }
    }

    static class ValidatorListenerImpl implements SimpleBeanValidatorListener {

        final NuitonValidatorScope scope;

        public ValidatorListenerImpl(NuitonValidatorScope scope) {
            this.scope = scope;
        }

        List<String> messages = Lists.newArrayList();

        public List<String> getMessages() {
            return messages;
        }

        @Override
        public void onFieldChanged(SimpleBeanValidatorEvent event) {
            if (scope == event.getScope()) {
                String[] messagesToDelete = event.getMessagesToDelete();
                if (messagesToDelete != null && messagesToDelete.length > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug(event.getScope() + " messages to delete : " + Arrays.toString(messagesToDelete));
                    }
                    for (String m : messagesToDelete) {
                        messages.remove(m);
                    }
                }
                String[] messagesToAdd = event.getMessagesToAdd();
                if (messagesToAdd != null && messagesToAdd.length > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug(event.getScope() + " messages to add : " + Arrays.toString(messagesToAdd));
                    }
                    messages.addAll(Arrays.asList(messagesToAdd));
                }
            }
        }
    }
}
