package org.nuiton.validator.bean.list;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.SimpleBean;

import java.util.Arrays;
import java.util.List;

/**
 * To test the {@link BeanListValidator}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public class BeanListValidatorTest {

    /** Logger */
    private static final Log log =
            LogFactory.getLog(BeanListValidatorTest.class);

    protected BeanListValidator<SimpleBean> validator;

    protected SimpleBean bean;

    protected SimpleBean bean2;

    BeanValidatorListenerImpl fatalListener;

    BeanValidatorListenerImpl errorListener;

    BeanValidatorListenerImpl warningListener;

    BeanValidatorListenerImpl infoListener;

    @Before
    public void setUp() {

        bean = new SimpleBean();
        bean2 = new SimpleBean();
    }

    protected void prepareValidator(String context) {

        validator = BeanListValidator.newValidator(SimpleBean.class, context);

        validator.addBeanListValidatorListener(fatalListener = new BeanValidatorListenerImpl(NuitonValidatorScope.FATAL));
        validator.addBeanListValidatorListener(errorListener = new BeanValidatorListenerImpl(NuitonValidatorScope.ERROR));
        validator.addBeanListValidatorListener(warningListener = new BeanValidatorListenerImpl(NuitonValidatorScope.WARNING));
        validator.addBeanListValidatorListener(infoListener = new BeanValidatorListenerImpl(NuitonValidatorScope.INFO));
    }

    @After
    public void tearDown() {
        bean = null;
        bean2 = null;
        if (validator != null) {
            validator.removeAllBeans();
            validator = null;
        }
    }

    private static final String STRING_VALUE_FATAL = "stringValue.fatal";

    private static final String STRING_VALUE_ERROR = "stringValue.error";

    private static final String STRING_VALUE_WARNING = "stringValue.warning";

    private static final String INT_VALUE_FATAL = "intValue.fatal";

    private static final String INT_VALUE_ERROR = "intValue.error";

    private static final String INT_VALUE_INFO = "intValue.info";

    @Test(expected = IllegalStateException.class)
    public void testValidateWithBad() {

        // with marchepo context, there is a unknown field in scope error

        prepareValidator("marchepo");
    }

    @Test
    public void addBean() {
        prepareValidator(null);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        validator.addBean(bean);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        validator.addBean(bean2);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

    }

    @Test
    public void addAllBean() {
        prepareValidator(null);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);

        validator.addAllBeans(Arrays.asList(bean, bean2));

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

    }

    @Test
    public void removeBean() {
        prepareValidator(null);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);

        validator.addBean(bean);
        validator.addBean(bean2);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

        validator.removeBean(bean);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        validator.removeBean(bean2);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);
    }

    @Test
    public void removeAllBeans() {
        prepareValidator(null);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);

        validator.addBean(bean);
        validator.addBean(bean2);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

        validator.removeAllBeans(Arrays.asList(bean, bean2));

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);
    }

    @Test
    public void removeAllBeans2() {
        prepareValidator(null);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);

        validator.addBean(bean);
        validator.addBean(bean2);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

        validator.removeAllBeans();

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        assertMessages(fatalListener, bean2);
        assertMessages(errorListener, bean2);
        assertMessages(warningListener, bean2);
        assertMessages(infoListener, bean2);
    }

    @Test
    public void validate() {

        prepareValidator(null);

        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }
        validator.addBean(bean);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }
        bean.setStringValue("one");

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }
        bean.setStringValue("oneone");

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, INT_VALUE_ERROR);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }
        bean.setIntValue(1);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }
        bean.setIntValue(10);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }

        bean.setStringValue(null);
        bean.setIntValue(0);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);


        if (log.isDebugEnabled()) {
            log.debug("- bean 1 ----------------------------------------------");
        }

        bean.setStringValue("5");
        bean.setIntValue(5);
        assertMessages(fatalListener, bean);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("- bean 2 ----------------------------------------------");
        }
        validator.addBean(bean2);

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("- bean 2 ----------------------------------------------");
        }
        bean2.setStringValue("one");

        assertMessages(fatalListener, bean2, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean2, INT_VALUE_ERROR);
        assertMessages(warningListener, bean2, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean2, INT_VALUE_INFO);

    }

    @Test
    public void convert() {

        prepareValidator(null);

        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean);
        assertMessages(infoListener, bean);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        validator.addBean(bean);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);


        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        Object value = validator.convert(bean, "intValue", "abc", Class.class);

        Assert.assertNull(value);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, STRING_VALUE_ERROR, "error.convertor.class");
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
        bean.setStringValue("one");

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, "error.convertor.class");
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        value = validator.convert(bean, "intValue", "3", Integer.class);

        bean.setIntValue((Integer) value);

        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }

        bean.setIntValue(-1);
        assertMessages(fatalListener, bean, STRING_VALUE_FATAL, INT_VALUE_FATAL);
        assertMessages(errorListener, bean, INT_VALUE_ERROR);
        assertMessages(warningListener, bean, STRING_VALUE_WARNING);
        assertMessages(infoListener, bean, INT_VALUE_INFO);

        if (log.isDebugEnabled()) {
            log.debug("-----------------------------------------------");
        }
    }

    void assertMessages(BeanValidatorListenerImpl listener,
                        Object bean,
                        String... expected) {
        List<String> actual = listener.getMessages(bean);
        Assert.assertEquals(" shoudl have " +
                            Arrays.toString(expected) + " but had " + actual,
                            expected.length, actual.size());
        for (String m : expected) {
            Assert.assertEquals("could not find " + m + " in " + actual,
                                true, actual.contains(m));
        }
    }

    class BeanValidatorListenerImpl implements BeanListValidatorListener {

        private final NuitonValidatorScope scope;

        private final ArrayListMultimap<Object, String> messages;

        public BeanValidatorListenerImpl(NuitonValidatorScope scope) {
            this.scope = scope;
            messages = ArrayListMultimap.create();
        }


        public ArrayListMultimap<Object, String> getMessages() {
            return messages;
        }

        public List<String> getMessages(Object bean) {
            return messages.get(bean);
        }


        @Override
        public void onFieldChanged(BeanListValidatorEvent event) {
            if (scope != event.getScope()) {
                return;
            }
            Object bean = event.getBean();
            String[] messagesToDelete = event.getMessagesToDelete();
            if (messagesToDelete != null && messagesToDelete.length > 0) {
                if (log.isDebugEnabled()) {
                    log.debug(scope + "[" + bean + "] messages to delete : " + Arrays.toString(messagesToDelete));
                }
                for (String m : messagesToDelete) {
                    messages.remove(bean, m);
                }
            }
            String[] messagesToAdd = event.getMessagesToAdd();
            if (messagesToAdd != null && messagesToAdd.length > 0) {
                if (log.isDebugEnabled()) {
                    log.debug(scope + "[" + bean + "] messages to add : " + Arrays.toString(messagesToAdd));
                }
                messages.putAll(bean, Arrays.asList(messagesToAdd));
            }
        }
    }
}
