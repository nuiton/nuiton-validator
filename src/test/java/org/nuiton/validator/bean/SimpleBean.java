/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class SimpleBean {

    protected int intValue;

    protected String stringValue;

    final PropertyChangeSupport p;

    public SimpleBean() {
        p = new PropertyChangeSupport(this);
    }

    public int getIntValue() {
        return intValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        String old = this.stringValue;
        this.stringValue = stringValue;
        p.firePropertyChange("stringValue", old, stringValue);
    }

    public void setIntValue(int intValue) {
        int old = this.intValue;
        this.intValue = intValue;
        p.firePropertyChange("intValue", old, intValue);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        p.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        p.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        p.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        p.removePropertyChangeListener(propertyName, listener);
    }
}
