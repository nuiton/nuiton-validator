/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.validator.model.Person;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

import java.util.Map;

/**
 * To test {@link NuitonValidatorFactory}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class NuitonValidatorFactoryTest {

    @Test
    public void testGetProviders() throws Exception {
        Map<String, NuitonValidatorProvider> providers = NuitonValidatorFactory.getProviders();
        Assert.assertNotNull(providers);
        Assert.assertEquals(1, providers.size());
        Assert.assertTrue(providers.containsKey(XWork2NuitonValidatorProvider.PROVIDER_NAME));
        Assert.assertTrue(providers.get(XWork2NuitonValidatorProvider.PROVIDER_NAME) instanceof XWork2NuitonValidatorProvider);
    }

    @Test
    public void testNewValidator() throws Exception {

        NuitonValidator<Person> validator =
                NuitonValidatorFactory.newValidator(Person.class);

        ValidatorTestHelper.testPerson(validator);
    }

}
