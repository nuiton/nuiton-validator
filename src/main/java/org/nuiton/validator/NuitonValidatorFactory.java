/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.ServiceLoader;
import java.util.TreeMap;

/**
 * Factory to obtain new validators.
 *
 * The factory contains a cache of {@link NuitonValidatorModel}.
 *
 * To obtain a new validator with no context, use this code :
 * <pre>
 * NuitonValidator&lt;O&gt; validator = NuitonValidatorFactory.newValidator(O.class);
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class NuitonValidatorFactory {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(NuitonValidatorFactory.class);

    protected static String defaultProviderName;

    protected static Map<String, NuitonValidatorProvider> providers;

    public static <O> NuitonValidator<O> newValidator(Class<O> type,
                                                      NuitonValidatorScope... scopes) {
        NuitonValidator<O> result = newValidator(type, null, scopes);
        return result;
    }

    public static <O> NuitonValidator<O> newValidator(Class<O> type,
                                                      String context,
                                                      NuitonValidatorScope... scopes) {

        String providerName = getDefaultProviderName();

        NuitonValidator<O> result = newValidator(providerName,
                                                 type,
                                                 context,
                                                 scopes);
        return result;
    }

    public static <O> NuitonValidator<O> newValidator(String providerName,
                                                      Class<O> type,
                                                      String context,
                                                      NuitonValidatorScope... scopes) throws NullPointerException {

        if (type == null) {
            throw new NullPointerException(
                    "type parameter can not be null.");
        }

        // get the provider
        NuitonValidatorProvider provider = getProvider(providerName);


        // obtain validator model form the provider
        NuitonValidatorModel<O> model =
                provider.getModel(type, context, scopes);

        // obtain validator from the the provider
        NuitonValidator<O> result = provider.newValidator(model);
        return result;
    }

    public static Map<String, NuitonValidatorProvider> getProviders() {
        if (providers == null) {
            providers = new TreeMap<String, NuitonValidatorProvider>();
            ServiceLoader<NuitonValidatorProvider> serviceLoader =
                    ServiceLoader.load(NuitonValidatorProvider.class);

            for (NuitonValidatorProvider provider : serviceLoader) {
                if (log.isInfoEnabled()) {
                    log.info("obtain validator provider " + provider.getName());
                }
                providers.put(provider.getName(), provider);
            }
        }
        return providers;
    }

    public static NuitonValidatorProvider getProvider(String providerName) throws IllegalArgumentException, NullPointerException {

        if (providerName == null) {

            // take the default validator provider name
            throw new NullPointerException(
                    "providerName parameter can not be null.");
        }

        NuitonValidatorProvider provider = getProviders().get(providerName);
        if (provider == null) {
            throw new IllegalArgumentException(
                    "Could not find provider named '" +
                    defaultProviderName + "', existing providers are : " +
                    getProviders().keySet());
        }
        return provider;
    }

    public static NuitonValidatorProvider getDefaultProvider() {
        String providerName = getDefaultProviderName();
        NuitonValidatorProvider provider = getProvider(providerName);
        return provider;
    }

    public static String getDefaultProviderName() throws IllegalStateException {

        if (defaultProviderName == null) {

            // takes the first provider from existing provider

            Map<String, NuitonValidatorProvider> providers = getProviders();
            if (providers.isEmpty()) {
                throw new IllegalStateException(
                        "Could not find any provider of validator.");
            }
            defaultProviderName = providers.keySet().iterator().next();

            if (log.isInfoEnabled()) {
                log.info("Set the default provider name to " + defaultProviderName);
            }
        }

        return defaultProviderName;
    }

    public static void setDefaultProviderName(String defaultProviderName) throws IllegalArgumentException, NullPointerException {

        if (defaultProviderName == null) {
            throw new NullPointerException("defaultProviderName can not be null.");
        }
        // check provider exists
        getProvider(defaultProviderName);

        NuitonValidatorFactory.defaultProviderName = defaultProviderName;
    }

    protected NuitonValidatorFactory() {
        // avoid instanciation of this factory
    }


}
