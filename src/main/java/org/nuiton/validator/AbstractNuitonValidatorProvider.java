/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract provider of validator.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public abstract class AbstractNuitonValidatorProvider implements NuitonValidatorProvider {

    protected Map<ModelEntry<?>, NuitonValidatorModel<?>> models;

    protected final String name;

    public AbstractNuitonValidatorProvider(String name) {
        this.name = name;
    }

    @Override
    public <O> NuitonValidatorModel<O> getModel(Class<O> type, String context, NuitonValidatorScope... scopes) {
        ModelEntry<O> key = ModelEntry.createModelEntry(type, context, scopes);

        @SuppressWarnings({"unchecked"})
        NuitonValidatorModel<O> model = (NuitonValidatorModel<O>) getModels().get(key);

        if (model == null) {
            model = newModel(type, context, scopes);
        }
        return model;
    }

    protected Map<ModelEntry<?>, NuitonValidatorModel<?>> getModels() {
        if (models == null) {
            models = new HashMap<ModelEntry<?>, NuitonValidatorModel<?>>();
        }
        return models;
    }

    protected static class ModelEntry<O> implements Serializable {

        private static final long serialVersionUID = 1L;

        protected final Class<O> type;

        protected final String context;

        protected final NuitonValidatorScope[] scopes;

        public static <O> ModelEntry<O> createModelEntry(Class<O> type,
                                                         String context,
                                                         NuitonValidatorScope... scopes) {
            return new ModelEntry<O>(type, context, scopes);
        }

        protected ModelEntry(Class<O> type,
                             String context,
                             NuitonValidatorScope... scopes) {
            this.type = type;
            this.context = context;
            this.scopes = scopes == null ? NuitonValidatorScope.values() : scopes;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ModelEntry)) return false;

            ModelEntry<?> that = (ModelEntry<?>) o;

            if (context != null ? !context.equals(that.context) : that.context != null)
                return false;
            if (!Arrays.equals(scopes, that.scopes)) return false;
            return type.equals(that.type);

        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + (context != null ? context.hashCode() : 0);
            result = 31 * result + Arrays.hashCode(scopes);
            return result;
        }
    }

    @Override
    public String getName() {
        return name;
    }

}
