/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Package of Nuiton-validator api.
 *
 * <h1>The <b>Validator</b> api</h1>
 * <p>
 * The {@link org.nuiton.validator.NuitonValidator} is the object responsible
 * to launch validation for a given object and then return the result of
 * validation in a {@link org.nuiton.validator.NuitonValidatorResult} via the
 * method {@link org.nuiton.validator.NuitonValidator#validate(Object)}.
 * </p>
 *
 * <pre>
 * NuitonValidator&lt;O&gt; validator = XXX;
 * NuitonValidatorResult result = validator.validate(o);
 * </pre>
 *
 * <h2>Obtain a validator</h2>
 * To obtain a validator use the factory of validators : {@link org.nuiton.validator.NuitonValidatorFactory}.
 * <pre>
 * NuitonValidator&lt;O&gt; validator = NuitonValidatorFactory.newValidator(O.class);
 * </pre>
 *
 * <h2>Implements the validator api</h2>
 *
 * At the moment, there is an offered implementation based on xwork2 framework.
 *
 * <strong>To be continued...</strong>
 *
 * @since 2.0
 */
package org.nuiton.validator;
