/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Represents the model of a {@link NuitonValidator}.
 *
 * This model describing properties of a validator :
 * <ul>
 * <li>{@link #type} : the type of object which can be validated by the validator</li>
 * <li>{@link #context} : the context of validation, if no context is required then the context is {@code null}.
 * <li>{@link #scopes} : the scopes of validation (see {@link NuitonValidatorScope})</li>
 * <li>{@link #fields} : the fields that can be validated by the validator</li>
 * </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class NuitonValidatorModel<O> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** Type of object to validate */
    protected Class<O> type;

    /** Context of validation (can be {@code null}, for no context). */
    protected String context;

    /** Set of scopes that can be validated for the type and context */
    protected Set<NuitonValidatorScope> scopes;

    /** Set of fields that can be validated for the type and context */
    protected Map<NuitonValidatorScope, String[]> fields;

    public NuitonValidatorModel(Class<O> type,
                                String context,
                                Set<NuitonValidatorScope> scopes,
                                Map<NuitonValidatorScope, String[]> fields) {
        this.type = type;
        this.context = context;
        this.scopes = Collections.unmodifiableSet(scopes);
        this.fields = Collections.unmodifiableMap(fields);
    }

    public Class<O> getType() {
        return type;
    }

    public String getContext() {
        return context;
    }

    public Set<NuitonValidatorScope> getScopes() {
        return scopes;
    }

    public Map<NuitonValidatorScope, String[]> getFields() {
        return fields;
    }
}
