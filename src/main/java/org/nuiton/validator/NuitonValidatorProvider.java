/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import java.io.File;
import java.util.ServiceLoader;
import java.util.SortedSet;
import java.util.regex.Pattern;

/**
 * Provider of {@link NuitonValidator}.
 *
 * An implementation of a such class provides a implementation of a validator models
 * and also of validator.
 *
 * <b>Note:</b> Providers are used in the {@link NuitonValidatorFactory} and
 * should be registered via the {@link ServiceLoader} api.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see NuitonValidatorModel
 * @see NuitonValidator
 * @see ServiceLoader
 * @since 2.0
 */
public interface NuitonValidatorProvider {

    /**
     * Obtains the name of the provider.
     *
     * @return the name of the provider.
     */
    String getName();

    /**
     * Obtain a validator model, the model should be cached and not be
     * reinstanciated at each time a validator model is asked.
     *
     * @param type    type of the class to validate
     * @param context context of validation ({@code null} if no context)
     * @param scopes  filtered scope (if nothing given, then use all scopes)
     * @param <O>     type of the class to validate
     * @return the cached model of validation
     */
    <O> NuitonValidatorModel<O> getModel(Class<O> type,
                                         String context,
                                         NuitonValidatorScope... scopes);

    /**
     * Instanciate a new validator model for the given parameters.
     *
     * @param type    type of the class to validate
     * @param context context of validation ({@code null} if no context)
     * @param scopes  filtered scope (if nothing given, then use all scopes)
     * @param <O>     type of the class to validate
     * @return the new instanciated model of validation
     */
    <O> NuitonValidatorModel<O> newModel(Class<O> type,
                                         String context,
                                         NuitonValidatorScope... scopes);

    /**
     * Obtains a new validator for the given {@code model}.
     *
     * @param model the model of validator to use
     * @param <O>   type of class to validate
     * @return the new validator
     */
    <O> NuitonValidator<O> newValidator(NuitonValidatorModel<O> model);

    /**
     * Detects in the given directory validators.
     *
     * @param sourceRoot    root directory where to seek for validators
     * @param contextFilter the pattern of context to seek
     * @param scopes        scopes to seek (if none given, will seek for all scopes)
     * @param types         types of class to seek
     * @return the set of validators found
     */
    SortedSet<NuitonValidator<?>> detectValidators(File sourceRoot,
                                                   Pattern contextFilter,
                                                   NuitonValidatorScope[] scopes,
                                                   Class<?>... types);
}
