/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import java.util.Set;

/**
 * Contract of a validator.
 *
 * To obtain validator, see the {@link NuitonValidatorFactory} api.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see NuitonValidatorFactory
 * @since 2.0
 */
public interface NuitonValidator<O> {

    /**
     * Validates the given object and returns the result of validation.
     *
     * @param object the object to validate
     * @return the result of validation for the given object
     * @throws NullPointerException if object is {@code null}.
     */
    NuitonValidatorResult validate(O object) throws NullPointerException;

    /**
     * Obtains the model of the validator.
     *
     * @return the model of the validator
     */
    NuitonValidatorModel<O> getModel();

    /**
     * Obtains the set of effective scopes for the validator : means the very
     * scopes that the validator is dealing with.
     *
     * This is a subset of the model authorized scopes.
     *
     * @return the set of effective scopes of the validator
     */
    Set<NuitonValidatorScope> getEffectiveScopes();

    /**
     * Obtains the set of effective fields for the validator : means the very
     * fields validated by the validator.
     *
     * This is a sub set of fields of the object to validate.
     *
     * @return the set of effective fields of the validator
     */
    Set<String> getEffectiveFields();

    /**
     * Obtains the set of effective fields for the validator for the given scope
     * : means the very fields validated by the validator.
     *
     * This is a subset of effective fields of the validator.
     *
     * @param scope given scope to use
     * @return the set of effective fields of the validator for the given scope
     */
    Set<String> getEffectiveFields(NuitonValidatorScope scope);

}
