package org.nuiton.validator.bean;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.validator.bean.list.BeanListValidator;
import org.nuiton.validator.bean.list.BeanListValidatorEvent;

import java.util.EventListener;

/**
 * The definition of an event on {@link BeanListValidatorEvent}
 * to be fired by a {@link BeanListValidator}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public interface ValidatorListener<E extends AbstractValidatorEvent<?>> extends EventListener {

    /**
     * Invoked when the {@link E} detects some changes for a
     * given bean / field / scope.
     *
     * @param event the event
     */
    void onFieldChanged(E event);
}
