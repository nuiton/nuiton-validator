package org.nuiton.validator.bean;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.validator.NuitonValidatorProvider;
import org.nuiton.validator.NuitonValidatorScope;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public interface ValidatorCreator<V> {

    /**
     * Given the parameters, instanciate a new {@link V}.
     *
     * @param provider the delegate validator provider
     * @param type     the type of object to validate
     * @param context  the context of validation (can be {@code null})
     * @param scopes   scopes to use (if none given, will use all available scopes)
     * @param <O>      type of object to validate
     * @return the new instance of bean validator
     */
    <O> V newValidator(NuitonValidatorProvider provider,
                       Class<O> type,
                       String context,
                       NuitonValidatorScope... scopes

    );
}
