package org.nuiton.validator.bean;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.util.beans.BeanUtil;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorProvider;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.event.EventListenerList;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Set;

/**
 * TODO
 *
 * @param <O> type of bean to validate
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public abstract class AbstractValidator<O> {

    /**
     * Name of the bounded property {@code context}.
     *
     * @see #getContext()
     * @see #setContext(String)
     */
    public static final String CONTEXT_PROPERTY = "context";

    /**
     * Name of the bounded property {@code scopes}.
     *
     * @see #getScopes()
     * @see #setScopes(NuitonValidatorScope...)
     */
    public static final String SCOPES_PROPERTY = "scopes";

    /**
     * Name of the bounded property {@link #valid}.
     *
     * @see #valid
     * @see #isValid()
     * @see #setValid(boolean)
     */
    public static final String VALID_PROPERTY = "valid";

    /**
     * Name of the bounded property {@link #changed}.
     *
     * @see #changed
     * @see #isChanged()
     * @see #setChanged(boolean)
     */
    public static final String CHANGED_PROPERTY = "changed";

    /**
     * State to indicate that validator has changed since the last time bean was
     * setted.
     */
    protected boolean changed;

    /** State of the validator (is true if no errors of error scope is found). */
    protected boolean valid = true;

    /**
     * State to know if the validator can be used (we keep this state for
     * performance reasons : do not want to compute this value each time a
     * validation is asked...).
     */
    protected boolean canValidate = true;

    /** Listener that listens on bean modification. */
    protected final PropertyChangeListener l;

    /** delegate property change support */
    protected final PropertyChangeSupport pcs;

    /** A list of event listeners for this validators */
    protected final EventListenerList listenerList = new EventListenerList();

    /**
     * The provider of delegate validators.
     *
     * It will also produce validator model.
     *
     * @see NuitonValidatorProvider
     */
    protected final NuitonValidatorProvider validatorProvider;


    protected AbstractValidator(NuitonValidatorProvider validatorProvider,
                                Class<O> beanClass) {

        // check if given bean class is Javabean compiliant
        boolean javaBeanCompiliant = BeanUtil.isJavaBeanCompiliant(beanClass);
        Preconditions.checkState(
                javaBeanCompiliant,
                beanClass.getName() + " is not JavaBean compiliant (" +
                BeanUtil.ADD_PROPERTY_CHANGE_LISTENER + ", or " +
                BeanUtil.REMOVE_PROPERTY_CHANGE_LISTENER +
                " method not found).");

        this.validatorProvider = validatorProvider;

        pcs = new PropertyChangeSupport(this);

        l = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                O bean = (O) evt.getSource();

                // the bean has changed, replay validation
                doValidate(bean);
            }
        };
    }

    /**
     * Obtain the {@link #changed} property value.
     *
     * Returns {@code true} if bean was modified since last
     * time a bean was attached.
     *
     * @return {@code true} if bean was modified since last attachement of
     * a bean.
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * To force the value of the property {@link #changed}.
     *
     * @param changed flag to force reset of property {@link #changed}
     */
    public void setChanged(boolean changed) {
        this.changed = changed;

        // force the property to be fired (never pass the older value)
        firePropertyChange(CHANGED_PROPERTY, null, changed);
    }

    public boolean isCanValidate() {
        return canValidate;
    }

    public void setCanValidate(boolean canValidate) {
        this.canValidate = canValidate;
    }

    /**
     * Obtain the {@link #valid} property value.
     *
     * @return {@code true} if attached bean is valid (no error or fatal messages)
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Change the value of the {@link #valid} property.
     *
     * @param valid the new value of the property
     */
    public void setValid(boolean valid) {
        this.valid = valid;

        // force the property to be fired (never pass the older value)
        firePropertyChange(VALID_PROPERTY, null, valid);
    }


    public String getContext() {
        return getModel().getContext();
    }

    public void setContext(String context) {

        String oldContext = getContext();

        if (ObjectUtils.equals(context, oldContext)) {

            // same context do nothing
            return;
        }

        NuitonValidatorModel<O> model = getModel();

        // compute the new validator model
        NuitonValidatorScope[] scopes = model.getScopes().toArray(
                new NuitonValidatorScope[model.getScopes().size()]);

        rebuildDelegateValidator(
                model.getType(),
                context,
                scopes
        );

        firePropertyChange(CONTEXT_PROPERTY,
                           oldContext,
                           context
        );
    }

    public Set<NuitonValidatorScope> getScopes() {
        return getModel().getScopes();
    }

    public Set<NuitonValidatorScope> getEffectiveScopes() {
        return getDelegate().getEffectiveScopes();
    }

    public Set<String> getEffectiveFields() {
        return getDelegate().getEffectiveFields();
    }

    public Set<String> getEffectiveFields(NuitonValidatorScope scope) {
        return getDelegate().getEffectiveFields(scope);
    }

    public void setScopes(NuitonValidatorScope... scopes) {

        Set<NuitonValidatorScope> oldScopes = getScopes();

        rebuildDelegateValidator(
                getModel().getType(),
                getModel().getContext(),
                scopes
        );

        firePropertyChange(SCOPES_PROPERTY,
                           oldScopes,
                           scopes
        );
    }

    public abstract void doValidate();

    public abstract boolean hasFatalErrors();

    public abstract boolean hasErrors();

    public abstract boolean hasWarnings();

    public abstract boolean hasInfos();

    public abstract boolean isValid(String fieldName);

    public abstract NuitonValidatorScope getHighestScope(String field);

    public abstract <T> T convert(O bean, String fieldName, String value, Class<T> valueClass);

    protected abstract void doValidate(O bean);

    protected abstract NuitonValidator<O> getDelegate();

    protected abstract void rebuildDelegateValidator(Class<O> beanType,
                                                     String context,
                                                     NuitonValidatorScope... scopes);

    public Class<O> getType() {
        return getModel().getType();
    }

    /**
     * Test a the validator contains the field given his name
     *
     * @param fieldName the name of the searched field
     * @return <code>true</code> if validator contaisn this field,
     * <code>false</code> otherwise
     */
    public boolean containsField(String fieldName) {
        Set<String> effectiveFields = getDelegate().getEffectiveFields();
        boolean result = effectiveFields.contains(fieldName);
        return result;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
                                          PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
                                             PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public void firePropertyChange(String propertyName,
                                   Object oldValue,
                                   Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected NuitonValidatorModel<O> getModel() {
        return getDelegate().getModel();
    }
}
