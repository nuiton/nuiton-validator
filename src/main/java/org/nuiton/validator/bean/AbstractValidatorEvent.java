package org.nuiton.validator.bean;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.validator.NuitonValidatorScope;

import java.util.EventObject;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public abstract class AbstractValidatorEvent<V> extends EventObject {

    private static final long serialVersionUID = 1L;

    /** the field impacted by the validator */
    protected String field;

    /** the scope impacted by the event */
    protected NuitonValidatorScope scope;

    protected String[] messagestoAdd;

    protected String[] messagestoDelete;

    public abstract Object getBean();

    public AbstractValidatorEvent(V source,
                                  String field,
                                  NuitonValidatorScope scope,
                                  String[] messagestoAdd,
                                  String[] messagestoDelete) {
        super(source);
        this.field = field;
        this.scope = scope;
        this.messagestoAdd = messagestoAdd;
        this.messagestoDelete = messagestoDelete;
    }

    @Override
    public V getSource() {
        return (V) super.getSource();
    }

    public String[] getMessagesToAdd() {
        return messagestoAdd;
    }

    public String[] getMessagesToDelete() {
        return messagestoDelete;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getField() {
        return field;
    }

}
