package org.nuiton.validator.bean.list;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.AbstractValidatorEvent;

/**
 * Event to be fired when some messages changed on a given field / scope of a bean.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public class BeanListValidatorEvent extends AbstractValidatorEvent<BeanListValidator<?>> {

    private static final long serialVersionUID = 1L;

    /** the bean on which event occurs. */
    protected Object bean;

    public BeanListValidatorEvent(BeanListValidator<?> source,
                                  Object bean,
                                  String field,
                                  NuitonValidatorScope scope,
                                  String[] messagestoAdd,
                                  String[] messagestoDelete) {
        super(source, field, scope, messagestoAdd, messagestoDelete);
        this.bean = bean;
    }

    public Object getBean() {
        return bean;
    }

}
