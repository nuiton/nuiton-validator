/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * Package of Nuiton - BeanListValidator api.
 *
 * <h1>The <b>BeanValidator</b> api</h1>
 * <p>
 * The {@link org.nuiton.validator.bean.list.BeanListValidator} purpose is to validate
 * a list of bean, with a listener api to interact with outside world.
 * </p>
 * It is mainly used in GUI parts of an application (Jaxx-validator use it).
 *
 * The idea is to attach the bean to validate insed the validator, then the
 * validator listen any modification of the bean to revalidate it and fires
 * events when messages has changed on a field.
 *
 * <pre>
 * BeanListValidatorListener listener = new BeanListValidatorListener() {XXX};
 * BeanValidator&lt;O&gt; validator = XXX;
 * validator.addBeanListValidatorListener(listener);
 * validator.addBean(bean1);
 * validator.addBean(bean2);
 * </pre>
 *
 * <h2>Obtain a validator</h2>
 * To obtain a bean validator use one of the factory method on the
 * {@link org.nuiton.validator.bean.list.BeanListValidator}.
 * <pre>
 * BeanListValidator&lt;O&gt; validator = BeanListValidator.newValidator(O.class);
 * </pre>
 *
 * <strong>To be continued...</strong>
 *
 * @since 2.0
 */
package org.nuiton.validator.bean.list;
