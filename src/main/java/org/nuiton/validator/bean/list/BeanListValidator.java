package org.nuiton.validator.bean.list;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.BeanUtil;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorProvider;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.AbstractNuitonValidatorContext;
import org.nuiton.validator.bean.AbstractValidator;

import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * To validate a list of beans.
 *
 * Each bean of the list will be associated with a {@link NuitonValidatorContext}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public class BeanListValidator<O> extends AbstractValidator<O> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BeanListValidator.class);

    /**
     * Obtain a new {@link BeanListValidator} for the given parameters.
     *
     * <b>Note:</b> It will use the default provider of {@link NuitonValidator}
     *
     * @param type    type of bean to validate
     * @param context context of validation
     * @param scopes  authorized scopes (if {@code null}, will use all scopes)
     * @param <O>     type of bean to validate
     * @return the new instanciated {@link BeanListValidator}.
     * @throws NullPointerException if type is {@code null}
     * @see NuitonValidatorFactory#getDefaultProviderName()
     */
    public static <O> BeanListValidator<O> newValidator(Class<O> type,
                                                        String context,
                                                        NuitonValidatorScope... scopes) throws NullPointerException {


        // get the provider default name
        String providerName = NuitonValidatorFactory.getDefaultProviderName();

        // get the bean validator with this provider
        BeanListValidator<O> beanValidator = newValidator(providerName,
                                                          type,
                                                          context,
                                                          scopes
        );
        return beanValidator;
    }

    /**
     * Obtain a new {@link BeanListValidator} for the given parameters.
     *
     * <b>Note:</b> It will use the provider of {@link NuitonValidator}
     * defined by the {@code providerName}.
     *
     * @param providerName name of {@link NuitonValidator} to use
     * @param type         type of bean to validate
     * @param context      context of validation
     * @param scopes       authorized scopes (if {@code null}, will use all scopes)
     * @param <O>          type of bean to validate
     * @return the new instanciated {@link BeanListValidator}.
     * @throws NullPointerException if type is {@code null}
     * @see NuitonValidatorFactory#getProvider(String)
     */
    public static <O> BeanListValidator<O> newValidator(String providerName,
                                                        Class<O> type,
                                                        String context,
                                                        NuitonValidatorScope... scopes) throws NullPointerException {

        Preconditions.checkNotNull(type, "type parameter can not be null.");

        // get delegate validator provider
        NuitonValidatorProvider provider =
                NuitonValidatorFactory.getProvider(providerName);

        Preconditions.checkState(
                provider != null,
                "Could not find provider with name " + providerName);

        // create the new instance of bean validator
        BeanListValidator<O> validator = new BeanListValidator<O>(
                provider, type, context, scopes
        );

        return validator;
    }

    /**
     * Context for each bean registred.
     *
     * @since 2.5.2
     */
    protected final Map<O, NuitonValidatorContext<O>> contexts;

    /**
     * The delegate validator used to validate the bean.
     *
     * @since 2.5.2
     */
    protected NuitonValidator<O> delegate;

    public BeanListValidator(NuitonValidatorProvider validatorProvider,
                             Class<O> beanClass,
                             String context) {

        this(validatorProvider, beanClass,
             context,
             NuitonValidatorScope.values()
        );
    }

    public BeanListValidator(NuitonValidatorProvider validatorProvider,
                             Class<O> beanClass,
                             String context,
                             NuitonValidatorScope... scopes) {

        super(validatorProvider, beanClass);

        contexts = Maps.newHashMap();

        // build delegate validator
        rebuildDelegateValidator(beanClass, context, scopes);

        // context has changed
        firePropertyChange(CONTEXT_PROPERTY,
                           null,
                           context
        );

        // scopes has changed
        firePropertyChange(SCOPES_PROPERTY,
                           null,
                           scopes
        );
    }

    /**
     * Add a bean to validate.
     *
     * The bean can not be null, nor registered twice in the validator.
     *
     * @param bean the bean to attach (can not be {@code null}).
     */
    public void addBean(O bean) {

        // bean can not be null
        Preconditions.checkNotNull(bean);

        // can not register twice the same bean
        Preconditions.checkState(!contexts.containsKey(bean),
                                 "The bean " + bean +
                                 " is already registred in this validator.");

        if (log.isDebugEnabled()) {
            log.debug(this + " : " + bean);
        }

        // create validator for this bean
        NuitonValidator<O> validator = validatorProvider.newValidator(getModel());

        // register it
        NuitonValidatorContext<O> newcontext =
                new NuitonValidatorContext<O>(bean, validator);
        newcontext.setValidator(validator);
        newcontext.setBean(bean);

        contexts.put(bean, newcontext);

        setCanValidate(isCanValidate() && newcontext.isCanValidate());

        try {

            BeanUtil.addPropertyChangeListener(l, bean);
        } catch (Exception eee) {
            if (log.isInfoEnabled()) {
                log.info("Can't register as listener for bean " + bean.getClass() +
                         " for reason " + eee.getMessage(), eee);
            }
        }
        validate(bean);

        setChanged(false);
        setValid(isValid0());
    }

    public void addAllBeans(Collection<O> beansToAdd) {
        for (O bean : beansToAdd) {
            addBean(bean);
        }
    }

    /**
     * Remove the given bean from the validaotr.
     *
     * The bean can not be {@code null}, nor not has been previously
     * registred in this validator.
     *
     * @param bean the bean to unregister
     */
    public void removeBean(O bean) {

        // bean can not be null
        Preconditions.checkNotNull(bean);

        if (log.isDebugEnabled()) {
            log.debug(this + " : " + bean);
        }

        // remove all messages for all fields of the validator
        NuitonValidatorContext<O> context = getContext(bean);

        mergeMessages(context, null);

        contexts.remove(bean);

        try {
            BeanUtil.removePropertyChangeListener(l, bean);
        } catch (Exception eee) {
            if (log.isInfoEnabled()) {
                log.info("Can't unregister as listener for bean " + bean.getClass() +
                         " for reason " + eee.getMessage(), eee);
            }
        }
    }

    /**
     * Remove all the given beans fro this validator.
     *
     * Like in method {@link #removeBean(Object)}, each bean must be not
     * {@code null} and has been previously registred in this validator.
     *
     * @param beansToRemove beans to remove from this validator
     */
    public void removeAllBeans(Collection<O> beansToRemove) {
        for (O bean : beansToRemove) {
            removeBean(bean);
        }
    }

    /**
     * Shortcut method to unregister all previously registred beans from
     * this validator.
     */
    public void removeAllBeans() {
        Set<O> beansToRemove = getBeans();
        removeAllBeans(beansToRemove);
    }

    @Override
    public boolean hasFatalErrors() {
        boolean result = false;
        for (NuitonValidatorContext<O> context : contexts.values()) {
            result = context.hasFatalErrors();
            if (result) {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean hasErrors() {
        boolean result = false;
        for (NuitonValidatorContext<O> context : contexts.values()) {
            result = context.hasErrors();
            if (result) {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean hasWarnings() {
        boolean result = false;
        for (NuitonValidatorContext<O> context : contexts.values()) {
            result = context.hasWarnings();
            if (result) {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean hasInfos() {
        boolean result = false;
        for (NuitonValidatorContext<O> context : contexts.values()) {
            result = context.hasInfos();
            if (result) {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean isValid(String fieldName) {
        boolean result = true;

        for (NuitonValidatorContext<O> context : contexts.values()) {
            result = context.isValid(fieldName);
            if (!result) {
                break;
            }
        }
        return result;
    }

    @Override
    public NuitonValidatorScope getHighestScope(String field) {
        Set<NuitonValidatorScope> scopes = Sets.newHashSet();
        for (NuitonValidatorContext<O> context : contexts.values()) {
            scopes.add(context.getHighestScope(field));
        }
        NuitonValidatorScope scope = null;
        if (scopes.isEmpty()) {
            List<NuitonValidatorScope> scopeList = Lists.newArrayList(scopes);
            Collections.sort(scopeList);
            scope = scopeList.get(0);
        }
        return scope;
    }

    @Override
    public void doValidate() {
        validate();
        setValid(isValid0());
        setChanged(true);
    }

    /**
     * Convert a value.
     *
     * If an error occurs, then add an error in validator.
     *
     * @param <T>        the type of conversion
     * @param fieldName  the name of the bean property
     * @param value      the value to convert
     * @param valueClass the type of converted value
     * @return the converted value, or null if conversion was not ok
     */
    @Override
    public <T> T convert(O bean,
                         String fieldName,
                         String value,
                         Class<T> valueClass) {
        NuitonValidatorContext<O> context = getContext(bean);
        T convert = null;
        try {
            convert = context.convert(fieldName, value, valueClass);
        } catch (ConversionException e) {
            // must revalidate
            validate();
        }
        return convert;
    }

    public void addBeanListValidatorListener(BeanListValidatorListener listener) {
        listenerList.add(BeanListValidatorListener.class, listener);
    }

    public void removeBeanListValidatorListener(BeanListValidatorListener listener) {
        listenerList.remove(BeanListValidatorListener.class, listener);
    }

    public BeanListValidatorListener[] getBeanListValidatorListeners() {
        return listenerList.getListeners(BeanListValidatorListener.class);
    }

    public Set<O> getBeans() {
        return ImmutableSet.copyOf(contexts.keySet());
    }

    @Override
    protected void doValidate(O bean) {
        validate(bean);
        setValid(isValid0());
        setChanged(true);
    }

    @Override
    protected NuitonValidator<O> getDelegate() {
        return delegate;
    }

    @Override
    protected void rebuildDelegateValidator(Class<O> beanType,
                                            String context,
                                            NuitonValidatorScope... scopes) {

        // changing context could change fields definition
        // so dettach bean, must rebuild the fields

        // Dettach the bean before any thing, because with the new delegate
        // validator some old fields could not be used any longer, and then
        // listeners will never have the full reset of their model...

        // remove all validators.

        if (scopes == null || scopes.length == 0) {
            scopes = NuitonValidatorScope.values();
        }

        // compute the new validator model
        NuitonValidatorModel<O> model = validatorProvider.getModel(beanType,
                                                                   context,
                                                                   scopes
        );

        // remove old delegate validator
        delegate = validatorProvider.newValidator(model);
    }

    /**
     * il faut eviter le code re-intrant (durant une validation, une autre est
     * demandee). Pour cela on fait la validation dans un thread, et tant que la
     * premiere validation n'est pas fini, on ne repond pas aux solicitations.
     * Cette method est public pour permettre de force une validation par
     * programmation, ce qui est utile par exemple si le bean ne supporte pas
     * les {@link PropertyChangeListener}
     *
     * <b>Note:</b> la methode est protected et on utilise la methode
     * {@link #doValidate()} car la méthode ne modifie pas les etats
     * internes et cela en rend son utilisation delicate (le validateur entre
     * dans un etat incoherent par rapport aux messages envoyés).
     */
    protected void validate() {

        // on ne valide que si il y a un bean et que le resultat de la validation
        // pourra etre affiche quelque part
        if (isCanValidate()) {

            for (O bean : contexts.keySet()) {

                validate(bean);
            }
        }
    }

    protected void validate(O bean) {
        NuitonValidatorContext<O> validator = getContext(bean);
        NuitonValidatorResult result = validator.validate();
        mergeMessages(validator, result);
    }

    protected boolean isValid0() {
        boolean result = true;
        for (NuitonValidatorContext<O> context : contexts.values()) {
            result = context.isValid();
            if (!result) {
                break;
            }
        }
        return result;
    }

    protected void mergeMessages(NuitonValidatorContext<O> context,
                                 NuitonValidatorResult newMessages) {

        List<BeanListValidatorEvent> events = context.mergeMessages(
                this, newMessages);

        if (CollectionUtils.isNotEmpty(events)) {

            // send all messages
            for (BeanListValidatorEvent event : events) {
                fireFieldChanged(event);
            }
        }
    }

    protected void fireFieldChanged(BeanListValidatorEvent evt) {

        for (BeanListValidatorListener listener :
                listenerList.getListeners(BeanListValidatorListener.class)) {
            listener.onFieldChanged(evt);
        }
    }

    public NuitonValidatorContext<O> getContext(O bean) {
        NuitonValidatorContext<O> context = contexts.get(bean);
        Preconditions.checkState(
                context != null,
                "Bean " + bean + " was not register in this list validator");
        return context;
    }


    public static class NuitonValidatorContext<O> extends AbstractNuitonValidatorContext<O, BeanListValidator<O>, BeanListValidatorEvent> {

        public NuitonValidatorContext(O bean, NuitonValidator<O> validator) {
            setValidator(validator);
            setBean(bean);
        }

        @Override
        protected BeanListValidatorEvent createEvent(BeanListValidator<O> source,
                                                     O bean,
                                                     String field,
                                                     NuitonValidatorScope scope,
                                                     String[] toAdd,
                                                     String[] toDelete) {
            return new BeanListValidatorEvent(
                    source,
                    bean,
                    field,
                    scope,
                    toAdd,
                    toDelete
            );
        }
    }
}
