package org.nuiton.validator.bean.simple;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.validator.NuitonValidatorScope;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;

/**
 * Useful methods arond {@link SimpleBeanValidator}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.4
 */
public class SimpleBeanValidators {

    protected SimpleBeanValidators() {
        // no constructor on helper class
    }

    public static EnumSet<NuitonValidatorScope> getScopes(
            List<SimpleBeanValidatorMessage<?>> messages) {
        EnumSet<NuitonValidatorScope> result =
                EnumSet.noneOf(NuitonValidatorScope.class);
        for (SimpleBeanValidatorMessage<?> m : messages) {
            result.add(m.getScope());
        }
        return result;
    }

    public static EnumMap<NuitonValidatorScope, Integer> getScopesCount(
            List<SimpleBeanValidatorMessage<?>> messages) {
        EnumMap<NuitonValidatorScope, Integer> result =
                new EnumMap<NuitonValidatorScope, Integer>(NuitonValidatorScope.class);
        for (NuitonValidatorScope s : NuitonValidatorScope.values()) {
            result.put(s, 0);
        }
        for (SimpleBeanValidatorMessage<?> m : messages) {

            NuitonValidatorScope scope = m.getScope();

            result.put(scope, result.get(scope) + 1);
        }

        for (NuitonValidatorScope s : NuitonValidatorScope.values()) {
            if (result.get(s) == 0) {
                result.remove(s);
            }
        }
        return result;
    }
}
