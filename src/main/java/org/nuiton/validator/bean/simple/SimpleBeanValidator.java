/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.bean.simple;

import com.google.common.base.Preconditions;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.BeanUtil;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorProvider;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.AbstractNuitonValidatorContext;
import org.nuiton.validator.bean.AbstractValidator;

import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Validator for a javaBean object.
 *
 * A such validator is designed to validate to keep the validation of a bean,
 * means the bean is attached to the validator (via the context field {@link #context}.
 *
 * A such validator is also a JavaBean and you can listen his states
 * modifications via the classic java bean api.
 *
 * <strong>Note:</strong> The {@link SimpleBeanValidator} should never be used for
 * validation in a service approch since it needs to keep a reference to the
 * bean to validate.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see SimpleBeanValidatorListener
 * @since 2.5.2
 */
public class SimpleBeanValidator<O> extends AbstractValidator<O> {

    /**
     * Name of the bounded property {@code bean}.
     *
     * @see #getBean()
     * @see #setBean(Object)
     */
    public static final String BEAN_PROPERTY = "bean";

    /** Logger. */
    private static final Log log = LogFactory.getLog(SimpleBeanValidator.class);

    /**
     * Obtain a new {@link SimpleBeanValidator} for the given parameters.
     *
     * <b>Note:</b> It will use the default provider of {@link NuitonValidator}
     *
     * @param type    type of bean to validate
     * @param context context of validation
     * @param scopes  authorized scopes (if {@code null}, will use all scopes)
     * @param <O>     type of bean to validate
     * @return the new instanciated {@link SimpleBeanValidator}.
     * @throws NullPointerException if type is {@code null}
     * @see NuitonValidatorFactory#getDefaultProviderName()
     */
    public static <O> SimpleBeanValidator<O> newValidator(
            Class<O> type,
            String context,
            NuitonValidatorScope... scopes) throws NullPointerException {


        // get the provider default name
        String providerName = NuitonValidatorFactory.getDefaultProviderName();

        // get the bean validator with this provider
        SimpleBeanValidator<O> beanValidator = newValidator(providerName,
                                                            type,
                                                            context,
                                                            scopes
        );
        return beanValidator;
    }

    /**
     * Obtain a new {@link SimpleBeanValidator} for the given parameters.
     *
     * <b>Note:</b> It will use the provider of {@link NuitonValidator}
     * defined by the {@code providerName}.
     *
     * @param providerName name of {@link NuitonValidator} to use
     * @param type         type of bean to validate
     * @param context      context of validation
     * @param scopes       authorized scopes (if {@code null}, will use all scopes)
     * @param <O>          type of bean to validate
     * @return the new instanciated {@link SimpleBeanValidator}.
     * @throws NullPointerException if type is {@code null}
     * @see NuitonValidatorFactory#getProvider(String)
     */
    public static <O> SimpleBeanValidator<O> newValidator(
            String providerName,
            Class<O> type,
            String context,
            NuitonValidatorScope... scopes) throws NullPointerException {

        Preconditions.checkNotNull(type,
                                   "type parameter can not be null.");

        // get delegate validator provider
        NuitonValidatorProvider provider =
                NuitonValidatorFactory.getProvider(providerName);

        Preconditions.checkState(
                provider != null,
                "Could not find provider with name " + providerName);

        // create the new instance of bean validator
        SimpleBeanValidator<O> validator = new SimpleBeanValidator<O>(
                provider, type, context, scopes);

        return validator;
    }

    /**
     * Context of the registred bean to validate.
     *
     * @since 2.5.2
     */
    protected final NuitonValidatorContext<O> context;

    /**
     * To chain to another validator (acting as parent of this one).
     *
     * @since 2.5.2
     */
    protected SimpleBeanValidator<?> parentValidator;

    public SimpleBeanValidator(NuitonValidatorProvider validatorProvider,
                               Class<O> beanClass,
                               String context) {

        this(validatorProvider, beanClass,
             context,
             NuitonValidatorScope.values()
        );
    }

    public SimpleBeanValidator(NuitonValidatorProvider validatorProvider,
                               Class<O> beanClass,
                               String context,
                               NuitonValidatorScope... scopes) {

        super(validatorProvider, beanClass);

        this.context = new NuitonValidatorContext<O>();

        // build delegate validator
        rebuildDelegateValidator(beanClass, context, scopes);

        // context has changed
        firePropertyChange(CONTEXT_PROPERTY,
                           null,
                           context
        );

        // scopes has changed
        firePropertyChange(SCOPES_PROPERTY,
                           null,
                           scopes
        );
    }

    /**
     * Obtain the actual bean attached to the validator.
     *
     * @return the bean attached to the validor or {@code null} if no bean
     * is attached
     */
    public O getBean() {
        return context.getBean();
    }

    /**
     * Change the attached bean.
     *
     * As a side effect, the internal
     * {@link AbstractNuitonValidatorContext#messages} will be reset.
     *
     * @param bean the bean to attach (can be {@code null} to reset the
     *             validator).
     */
    public void setBean(O bean) {
        O oldBean = getBean();
        if (log.isDebugEnabled()) {
            log.debug(this + " : " + bean);
        }

        if (oldBean != null) {
            try {
                BeanUtil.removePropertyChangeListener(l, oldBean);
            } catch (Exception eee) {
                if (log.isInfoEnabled()) {
                    log.info("Can't unregister as listener for bean " + oldBean.getClass() +
                             " for reason " + eee.getMessage(), eee);
                }
            }
        }
        context.setBean(bean);

        if (bean == null) {

            // remove all messages for all fields of the validator

            mergeMessages(null);

        } else {
            try {

                BeanUtil.addPropertyChangeListener(l, bean);
            } catch (Exception eee) {
                if (log.isInfoEnabled()) {
                    log.info("Can't register as listener for bean " + bean.getClass() +
                             " for reason " + eee.getMessage(), eee);
                }
            }
            validate();
        }
        setChanged(false);
        setValid(context.isValid());
        firePropertyChange(BEAN_PROPERTY, oldBean, bean);
    }

    public SimpleBeanValidator<?> getParentValidator() {
        return parentValidator;
    }

    public void setParentValidator(SimpleBeanValidator<?> parentValidator) {
        this.parentValidator = parentValidator;
    }

    @Override
    public boolean hasFatalErrors() {
        boolean result = context.hasFatalErrors();
        return result;
    }

    @Override
    public boolean hasErrors() {
        boolean result = context.hasErrors();
        return result;
    }

    @Override
    public boolean hasWarnings() {
        boolean result = context.hasWarnings();
        return result;
    }

    @Override
    public boolean hasInfos() {
        boolean result = context.hasInfos();
        return result;
    }

    @Override
    public boolean isValid(String fieldName) {

        // field is valid if no fatal messages nor error messages
        boolean result = context.isValid(fieldName);
        return result;
    }

    @Override
    public NuitonValidatorScope getHighestScope(String field) {
        NuitonValidatorScope scope = context.getHighestScope(field);
        return scope;
    }

    @Override
    public <T> T convert(O bean, String fieldName, String value, Class<T> valueClass) {
        Preconditions.checkState(
                ObjectUtils.equals(bean, getBean()),
                "Can not validate the bean [" + bean +
                "] which is not the one registred [" + bean +
                "] in this validator.");

        T convert = convert(fieldName, value, valueClass);
        return convert;
    }

    @Override
    public void doValidate() {
        validate();
        setValid(context.isValid());
        setChanged(true);
    }

    /**
     * Convert a value.
     *
     * If an error occurs, then add an error in validator.
     *
     * @param <T>        the type of conversion
     * @param fieldName  the name of the bean property
     * @param value      the value to convert
     * @param valueClass the type of converted value
     * @return the converted value, or null if conversion was not ok
     */
    public <T> T convert(String fieldName, String value, Class<T> valueClass) {
        T convert = null;

        try {
            convert = context.convert(fieldName, value, valueClass);
        } catch (ConversionException e) {
            // must revalidate
            validate();
        }
        return convert;
    }

    public void addSimpleBeanValidatorListener(SimpleBeanValidatorListener listener) {
        listenerList.add(SimpleBeanValidatorListener.class, listener);
    }

    public void removeSimpleBeanValidatorListener(SimpleBeanValidatorListener listener) {
        listenerList.remove(SimpleBeanValidatorListener.class, listener);
    }

    public SimpleBeanValidatorListener[] getSimpleBeanValidatorListeners() {
        return listenerList.getListeners(SimpleBeanValidatorListener.class);
    }

    @Override
    protected void doValidate(O bean) {

        Preconditions.checkState(
                ObjectUtils.equals(bean, getBean()),
                "Can not validate the bean [" + bean +
                "] which is not the one registred [" + bean +
                "] in this validator.");

        doValidate();
    }

    @Override
    protected NuitonValidator<O> getDelegate() {
        return context.getValidator();
    }

    @Override
    protected void rebuildDelegateValidator(Class<O> beanType,
                                            String context,
                                            NuitonValidatorScope... scopes) {

        // changing context could change fields definition
        // so dettach bean, must rebuild the fields

        // Dettach the bean before any thing, because with the new delegate
        // validator some old fields could not be used any longer, and then
        // listeners will never have the full reset of their model...
        if (getBean() != null) {
            setBean(null);
        }

        if (scopes == null || scopes.length == 0) {
            scopes = NuitonValidatorScope.values();
        }

        // compute the new validator model
        NuitonValidatorModel<O> validatorModel = validatorProvider.getModel(beanType,
                                                                            context,
                                                                            scopes
        );

        // remove old delegate validator
        NuitonValidator<O> delegate = validatorProvider.newValidator(validatorModel);
        this.context.setValidator(delegate);
    }

    /**
     * il faut eviter le code re-intrant (durant une validation, une autre est
     * demandee). Pour cela on fait la validation dans un thread, et tant que la
     * premiere validation n'est pas fini, on ne repond pas aux solicitations.
     * Cette method est public pour permettre de force une validation par
     * programmation, ce qui est utile par exemple si le bean ne supporte pas
     * les {@link PropertyChangeListener}
     *
     * <b>Note:</b> la methode est protected et on utilise la methode
     * {@link #doValidate()} car la méthode ne modifie pas les etats
     * internes et cela en rend son utilisation delicate (le validateur entre
     * dans un etat incoherent par rapport aux messages envoyés).
     */
    protected void validate() {

        // on ne valide que si il y a un bean et que le resultat de la validation
        // pourra etre affiche quelque part
        if (isCanValidate()) {

            NuitonValidatorResult result = context.validate();

            mergeMessages(result);

            if (parentValidator != null) {
                // chained validation
                // the parent validator should not be changed from this validation
                boolean wasModified = parentValidator.isChanged();
                parentValidator.doValidate();
                if (!wasModified) {
                    // push back old state
                    parentValidator.setChanged(false);
                }
            }
        }
    }

    protected void fireFieldChanged(SimpleBeanValidatorEvent evt) {

        for (SimpleBeanValidatorListener listener :
                listenerList.getListeners(SimpleBeanValidatorListener.class)) {
            listener.onFieldChanged(evt);
        }
    }

    protected void mergeMessages(NuitonValidatorResult newMessages) {

        List<SimpleBeanValidatorEvent> events = context.mergeMessages(this,
                                                                      newMessages);

        if (CollectionUtils.isNotEmpty(events)) {

            // send all messages
            for (SimpleBeanValidatorEvent event : events) {
                fireFieldChanged(event);
            }
        }
    }

    protected static class NuitonValidatorContext<O> extends AbstractNuitonValidatorContext<O, SimpleBeanValidator<O>, SimpleBeanValidatorEvent> {

        @Override
        protected SimpleBeanValidatorEvent createEvent(SimpleBeanValidator<O> source,
                                                       O bean,
                                                       String field,
                                                       NuitonValidatorScope scope,
                                                       String[] toAdd,
                                                       String[] toDelete) {
            SimpleBeanValidatorEvent evt = new SimpleBeanValidatorEvent(
                    source,
                    field,
                    scope,
                    toAdd,
                    toDelete
            );
            return evt;
        }
    }
}
