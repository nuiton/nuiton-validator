package org.nuiton.validator.bean;
/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;
import org.nuiton.validator.bean.list.BeanListValidator;
import org.nuiton.validator.bean.simple.SimpleBeanValidator;

import java.beans.Introspector;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Defines a context of validation used for a single bean.
 *
 * {@link SimpleBeanValidator} will then used one of this object and
 * {@link BeanListValidator} as many as it contains beans.
 *
 * This object box a {@link NuitonValidator} to get validation state each time
 * a higher validator requires it.
 *
 * It also offers the way to create events (merge logic
 *
 * @param <O> type of bean to validate
 * @param <V> type of bean validator used
 * @param <E> type of event to create
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public abstract class AbstractNuitonValidatorContext<O, V, E> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AbstractNuitonValidatorContext.class);

    /** Bean to validate. */
    protected O bean;

    /**
     * State of validation (keep all messages of validation for the filled
     * bean).
     */
    protected NuitonValidatorResult messages;

    /** Validator. */
    protected NuitonValidator<O> validator;

    /** map of conversion errors detected by this validator */
    protected final Map<String, String> conversionErrors;

    /**
     * State to know if the validator can be used (we keep this state for
     * performance reasons : do not want to compute this value each time a
     * validation is asked...).
     */
    protected boolean canValidate;

    protected abstract E createEvent(V source,
                                     O bean,
                                     String field,
                                     NuitonValidatorScope scope,
                                     String[] toAdd,
                                     String[] toDelete);

    public AbstractNuitonValidatorContext() {
        conversionErrors = Maps.newTreeMap();
    }

    public O getBean() {
        return bean;
    }

    public void setBean(O bean) {
        if (log.isDebugEnabled()) {
            log.debug(this + " : " + bean);
        }

        // clean conversions of previous bean
        conversionErrors.clear();
        this.bean = bean;

        setCanValidate(!validator.getEffectiveFields().isEmpty() && bean != null);
    }

    public NuitonValidator<O> getValidator() {
        return validator;
    }

    public NuitonValidatorResult getMessages() {
        return messages;
    }

    public boolean isCanValidate() {
        return canValidate;
    }

    public void setCanValidate(boolean canValidate) {
        this.canValidate = canValidate;
    }

    public boolean isValid() {
        return messages == null || messages.isValid();
    }

    public boolean hasFatalErrors() {
        boolean result = messages != null && messages.hasFatalMessages();
        return result;
    }

    public boolean hasErrors() {
        boolean result = messages != null && messages.hasErrorMessagess();
        return result;
    }

    public boolean hasWarnings() {
        boolean result = messages != null && messages.hasWarningMessages();
        return result;
    }

    public boolean hasInfos() {
        boolean result = messages != null && messages.hasInfoMessages();
        return result;
    }

    public boolean isValid(String fieldName) {

        // field is valid if no fatal messages nor error messages
        boolean result = !(
                messages.hasMessagesForScope(fieldName, NuitonValidatorScope.FATAL) ||
                messages.hasMessagesForScope(fieldName, NuitonValidatorScope.ERROR));

        return result;
    }

    public NuitonValidatorScope getHighestScope(String field) {

        NuitonValidatorScope scope = messages.getFieldHighestScope(field);
        return scope;
    }

    public void setValidator(NuitonValidator<O> validator) {
        this.validator = validator;
    }

    public NuitonValidatorResult validate() {
        NuitonValidatorResult result = validator.validate(bean);

        // treate conversion errors
        // reinject them
        for (Map.Entry<String, String> entry : conversionErrors.entrySet()) {


            // remove from validation, errors occurs on this field
            String field = entry.getKey();


            List<String> errors = result.getErrorMessages(field);

            String conversionError = entry.getValue();
            if (errors != null) {
                errors.clear();
                errors.add(conversionError);
            } else {
                errors = Collections.singletonList(conversionError);
            }

            result.setMessagesForScope(NuitonValidatorScope.ERROR, field, errors);
        }
        return result;
    }

    /**
     * Convert a value.
     *
     * If an error occurs, then add an error in validator.
     *
     * @param <T>        the type of conversion
     * @param fieldName  the name of the bean property
     * @param value      the value to convert
     * @param valueClass the type of converted value
     * @return the converted value, or null if conversion was not ok
     */
    @SuppressWarnings({"unchecked"})
    public <T> T convert(String fieldName, String value, Class<T> valueClass) {
        if (fieldName == null) {
            throw new IllegalArgumentException("fieldName can not be null");
        }
        if (valueClass == null) {
            throw new IllegalArgumentException("valueClass can not be null");
        }

        // on ne convertit pas si il y a un bean et que le resultat de la
        // validation pourra etre affiche quelque part
        if (!isCanValidate() || value == null) {
            return null;
        }

        // remove the previous conversion error for the field
        conversionErrors.remove(fieldName);

        T result;
        try {
            Converter converter = ConverterUtil.getConverter(valueClass);
            if (converter == null) {
                throw new RuntimeException(
                        "could not find converter for the type " + valueClass);
            }
            result = converter.convert(valueClass, value);
            /* Why this test ? if (result != null && !value.equals(result.toString())) {
            conversionErrors.put(fieldName, "error.convertor." + Introspector.decapitalize(valueClass.getSimpleName()));
            result = null;
            validate();
            }*/
        } catch (ConversionException e) {
            // get
            String s = Introspector.decapitalize(valueClass.getSimpleName());
            conversionErrors.put(fieldName, "error.convertor." + s);
            throw e;
        }
        return result;
    }

    public List<E> mergeMessages(V beanValidator,
                                 NuitonValidatorResult newMessages) {

        if (newMessages == null && messages == null) {

            // no messages ever registred and ask to delete them, so nothing
            // to do
            return null;
        }

        Set<NuitonValidatorScope> scopes = getValidator().getEffectiveScopes();

        // list of events to send after the merge of messages
        List<E> events = Lists.newArrayList();

        for (NuitonValidatorScope scope : scopes) {

            // do the merge at scope level
            mergeMessages(beanValidator, scope, newMessages, events);

        }

        if (newMessages != null) {

            //TODO tchemit 2011-01-23 Perharps it will necessary to clear the messages for memory performance ?

            // finally keep the new messages as the current messages
            this.messages = newMessages;
        }

        return events;
    }

    protected void mergeMessages(V beanValidator,
                                 NuitonValidatorScope scope,
                                 NuitonValidatorResult newMessages,
                                 List<E> events) {


        if (newMessages == null) {

            // special case to empty all messages

            List<String> fieldsForScope = messages.getFieldsForScope(scope);

            for (String field : fieldsForScope) {
                List<String> messagesForScope = messages.getMessagesForScope(field, scope);
                events.add(createEvent(beanValidator, bean, field, scope, null, messagesForScope.toArray(new String[messagesForScope.size()])));
            }

            // suppress all messages for this scope
            messages.clearMessagesForScope(scope);


        } else {

            List<String> newFields = newMessages.getFieldsForScope(scope);

            if (messages == null) {

                // first time of a merge, just add new messages

                for (String field : newFields) {
                    List<String> messagesForScope = newMessages.getMessagesForScope(field, scope);
                    events.add(createEvent(beanValidator, bean, field, scope, messagesForScope.toArray(new String[messagesForScope.size()]), null));
                }

                // nothing else to do
                return;
            }

            List<String> oldFields = messages.getFieldsForScope(scope);

            Iterator<String> itr;

            // detects field with only new messages
            itr = newFields.iterator();
            while (itr.hasNext()) {
                String newField = itr.next();

                if (!oldFields.contains(newField)) {

                    // this fields has now messages but not before : new messages
                    List<String> messagesForScope = newMessages.getMessagesForScope(newField, scope);
                    events.add(createEvent(beanValidator, bean, newField, scope, messagesForScope.toArray(new String[messagesForScope.size()]), null));

                    // treated field
                    itr.remove();
                }
            }

            // detects fields with only obsolete messages
            itr = oldFields.iterator();
            while (itr.hasNext()) {
                String oldField = itr.next();

                if (!newFields.contains(oldField)) {

                    // this fields has no more messages
                    List<String> messagesForScope = messages.getMessagesForScope(oldField, scope);
                    events.add(createEvent(beanValidator, bean, oldField, scope, null, messagesForScope.toArray(new String[messagesForScope.size()])));

                    // treated field
                    itr.remove();
                }
            }

            // now deal with mixte field (toAdd and toDelete)
            for (String field : newFields) {

                List<String> newMessagesForScope = newMessages.getMessagesForScope(field, scope);
                List<String> oldMessagesForScope = messages.getMessagesForScope(field, scope);

                // get old obsoletes messages to delete
                Set<String> toDelete = new HashSet<String>(oldMessagesForScope);
                toDelete.removeAll(newMessagesForScope);

                // get new messages to add
                Set<String> toAdd = new HashSet<String>(newMessagesForScope);
                toAdd.removeAll(oldMessagesForScope);

                events.add(createEvent(
                        beanValidator,
                        bean,
                        field,
                        scope,
                        toAdd.isEmpty() ? null : toAdd.toArray(new String[toAdd.size()]),
                        toDelete.isEmpty() ? null : toDelete.toArray(new String[toDelete.size()])
                ));

            }
        }
    }
}
