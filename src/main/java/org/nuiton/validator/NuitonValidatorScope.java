/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator;

import static org.nuiton.i18n.I18n.n;

/**
 * The differents levels of messages in validation process.
 *
 * The order of the enum defines the severity of validation.
 *
 * Always begin with fatal, then error, then if no error found, try warning, then info...
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public enum NuitonValidatorScope {

    /**
     * the fatal error scope level.
     *
     * When a message of a such scope is found on a validator, then the
     * validator is invalid and modified.
     */
    FATAL(n("validator.scope.fatal.label")),
    /**
     * the error scope level.
     *
     * When a message of a such scope is found on a validator, then the
     * validator is invalid and modified.
     */
    ERROR(n("validator.scope.error.label")),
    /**
     * the warning scope level.
     *
     * When a message of a such scope is found on a validator, then the
     * validator is still valid but modified.
     */
    WARNING(n("validator.scope.warning.label")),
    /**
     * the information scope level.
     *
     * When a message of a sucg scope is found on a validator, then the
     * validator is still valid and not modified.
     */
    INFO(n("validator.scope.info.label"));

    private final String label;

    NuitonValidatorScope(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
