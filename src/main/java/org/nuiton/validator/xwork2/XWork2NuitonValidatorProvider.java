/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;
import org.nuiton.validator.AbstractNuitonValidatorProvider;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorScope;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provider of validator for the xworks nuiton validator.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class XWork2NuitonValidatorProvider extends AbstractNuitonValidatorProvider {

    public static final String PROVIDER_NAME = "xwork2";

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(XWork2NuitonValidatorProvider.class);

    public XWork2NuitonValidatorProvider() {
        super(PROVIDER_NAME);
    }

    @Override
    public <O> NuitonValidatorModel<O> newModel(Class<O> type,
                                                String context,
                                                NuitonValidatorScope... scopes) {

        if (scopes.length == 0) {
            // use all scopes
            scopes = NuitonValidatorScope.values();
        }

        Map<NuitonValidatorScope, String[]> fields =
                XWork2ValidatorUtil.detectFields(type, context, scopes);

        Set<NuitonValidatorScope> scopeSet =
                EnumSet.noneOf(NuitonValidatorScope.class);
        scopeSet.addAll(Arrays.asList(scopes));

        return new NuitonValidatorModel<O>(type, context, scopeSet, fields);
    }

    @Override
    public <O> XWork2NuitonValidator<O> newValidator(NuitonValidatorModel<O> model) {
        return new XWork2NuitonValidator<O>(model);
    }

    @Override
    public SortedSet<NuitonValidator<?>> detectValidators(File sourceRoot,
                                                          Pattern contextFilter,
                                                          NuitonValidatorScope[] scopes,
                                                          Class<?>... types) {

        if (scopes == null) {

            // use all scopes
            scopes = NuitonValidatorScope.values();
        }

        SortedSet<NuitonValidator<?>> result =
                new TreeSet<NuitonValidator<?>>(new ValidatorComparator());

        for (Class<?> c : types) {
            File dir = getClassDir(sourceRoot, c);
            if (!dir.exists()) {

                // pas de repertoire adequate
                if (log.isDebugEnabled()) {
                    log.debug("skip none existing directory " + dir);
                }
                continue;
            }
            String[] contexts = getContexts(c, dir);
            if (log.isDebugEnabled()) {
                log.debug("contexts : " + Arrays.toString(contexts));
            }

            if (contexts.length > 0) {
                String[] realContexts = getContextsWithoutScopes(contexts);

                if (log.isDebugEnabled()) {
                    log.debug("realContexts : " +
                              Arrays.toString(realContexts));
                }

                if (contextFilter != null) {

                    // filter contexts
                    realContexts = getFilterContexts(contextFilter,
                                                     realContexts
                    );
                    if (log.isDebugEnabled()) {
                        log.debug("filterContexts : " +
                                  Arrays.toString(realContexts));
                    }
                }

                for (String context : realContexts) {

                    // on cherche le validateur
                    NuitonValidator<?> validator = getValidator(
                            c,
                            context.isEmpty() ? null : context,
                            scopes
                    );
                    if (validator != null) {
                        // on enregistre le validateur
                        result.add(validator);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Pour un context et un type d'entité donné, instancie un validateur et
     * test si ce validateur est utilisable (i.e qu'il admet des champs à
     * valider).
     *
     * Si aucun champ n'est trouvé dans le validateur, alors on retourne null.
     *
     * @param <O>     le type du bean
     * @param klass   le type du bean
     * @param context le context du validateur
     * @param scopes  les scopes a utiliser (si {@code null} alors pas de
     *                filtre sur les scopes)
     * @return le validateur initialisé, ou <code>null</code> si aucun scope
     * détecté dans le validateur.
     */
    protected <O> NuitonValidator<O> getValidator(Class<O> klass,
                                                  String context,
                                                  NuitonValidatorScope... scopes) {

        NuitonValidatorModel<O> model = newModel(klass, context, scopes);

        XWork2NuitonValidator<O> valitator = newValidator(model);

        Set<NuitonValidatorScope> realScopes = valitator.getEffectiveScopes();
        if (realScopes.isEmpty()) {
            valitator = null;
            if (log.isDebugEnabled()) {
                log.debug(klass + " : validator skip (no scopes detected)");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug(klass + " : keep validator " + valitator);
            }
        }
        return valitator;
    }

    protected File getClassDir(File sourceRoot, Class<?> clazz) {
        String path = clazz.getPackage().getName();
        path = path.replaceAll("\\.", StringUtil.getFileSeparatorRegex());
        File dir = new File(sourceRoot, path);
        return dir;
    }

    protected String[] getContexts(Class<?> clazz, File dir) {
        Set<String> result = new TreeSet<String>();
        ValidatorFilenameFilter filter = new ValidatorFilenameFilter(clazz);
        if (log.isDebugEnabled()) {
            log.debug("dir : " + dir);
        }
        String[] files = dir.list(filter);
        for (String file : files) {
            if (log.isDebugEnabled()) {
                log.debug("file " + file);
            }
            String context = file.substring(
                    filter.prefix.length(),
                    file.length() - ValidatorFilenameFilter.SUFFIX.length()
            );
            if (log.isDebugEnabled()) {
                log.debug("detect " + clazz.getSimpleName() +
                          " context [" + context + "]");
            }
            result.add(context);
        }
        return result.toArray(new String[result.size()]);
    }

    protected String[] getContextsWithoutScopes(String[] contexts) {
        Set<String> result = new TreeSet<String>();
        NuitonValidatorScope[] scopes = NuitonValidatorScope.values();
        for (String context : contexts) {
            for (NuitonValidatorScope scope : scopes) {
                String scopeName = scope.name().toLowerCase();
                if (!context.endsWith(scopeName)) {
                    // pas concerne par ce scope
                    continue;
                }
                if (log.isDebugEnabled()) {
                    log.debug("detect context : " + context);
                }
                String realContext = context.substring(
                        0,
                        context.length() - scopeName.length()
                );
                if (realContext.endsWith("-")) {
                    realContext = realContext.substring(
                            0,
                            realContext.length() - 1
                    );
                }
                result.add(realContext);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    protected String[] getFilterContexts(Pattern contextFilter,
                                         String[] realContexts) {
        List<String> result = new ArrayList<String>();
        for (String c : realContexts) {
            Matcher m = contextFilter.matcher(c);
            if (m.matches()) {
                result.add(c);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    protected static class ValidatorFilenameFilter implements FilenameFilter {

        protected static final String SUFFIX = "-validation.xml";

        protected Class<?> clazz;

        protected String prefix;

        public ValidatorFilenameFilter(Class<?> clazz) {
            this.clazz = clazz;
            prefix = clazz.getSimpleName() + "-";
        }

        @Override
        public boolean accept(File dir, String name) {
            boolean result = name.endsWith(SUFFIX);
            if (result) {
                result = name.startsWith(prefix);
            }
            return result;
        }
    }

    protected static class ValidatorComparator implements Comparator<NuitonValidator<?>> {

        @Override
        public int compare(NuitonValidator<?> o1, NuitonValidator<?> o2) {

            NuitonValidatorModel<?> model1 = ((XWork2NuitonValidator<?>) o1).getModel();
            NuitonValidatorModel<?> model2 = ((XWork2NuitonValidator<?>) o2).getModel();

            String contextName1 =
                    model1.getType().getSimpleName() + "-" +
                    (model1.getContext() == null ? "" : model1.getContext());
            String contextName2 =
                    model2.getType().getSimpleName() + "-" +
                    (model2.getContext() == null ? "" : model2.getContext());
            return contextName1.compareTo(contextName2);
        }
    }

}
