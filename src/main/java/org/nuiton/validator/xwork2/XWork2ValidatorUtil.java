/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2;

import com.google.common.collect.Maps;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.ActionProxyFactory;
import com.opensymphony.xwork2.DefaultActionInvocation;
import com.opensymphony.xwork2.ObjectFactory;
import com.opensymphony.xwork2.Result;
import com.opensymphony.xwork2.UnknownHandler;
import com.opensymphony.xwork2.XWorkException;
import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.config.ConfigurationManager;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.opensymphony.xwork2.inject.Container;
import com.opensymphony.xwork2.inject.Inject;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.ValueStackFactory;
import com.opensymphony.xwork2.validator.ActionValidatorManager;
import com.opensymphony.xwork2.validator.FieldValidator;
import com.opensymphony.xwork2.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.BeanUtil;
import org.nuiton.validator.NuitonValidatorScope;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Usefull methods to works with work2 validator api.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class XWork2ValidatorUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(XWork2ValidatorUtil.class);

    /**
     * A shared value stack to allow external operations on it (for example add
     * some datas in stack to be used by validators.
     */
    static private ValueStack sharedValueStack;

    public static ValueStack getSharedValueStack() {
        if (sharedValueStack == null) {

            // init context
            sharedValueStack = createValuestack();
            if (log.isDebugEnabled()) {
                log.debug("init shared value stack " + sharedValueStack);
            }
        }
        return sharedValueStack;
    }

    /**
     * Sets the given value stack as shared (can be null).
     *
     * @param sharedValueStack the new shared value stack to use (can be null).
     * @since 3.0
     */
    public static void setSharedValueStack(ValueStack sharedValueStack) {
        if (log.isDebugEnabled()) {
            log.debug("set shared value stack " + sharedValueStack);
        }
        XWork2ValidatorUtil.sharedValueStack = sharedValueStack;
    }

    public static ValueStack createValuestack() {

        ValueStack result;

        ActionContext context = ActionContext.getContext();
        if (context == null) {

            // no action context, create a value stack from scratch
            ConfigurationManager confManager = new ConfigurationManager();
            Configuration conf = confManager.getConfiguration();
            Container container = conf.getContainer();
            ValueStackFactory stackFactory = container.getInstance(ValueStackFactory.class);
            result = stackFactory.createValueStack();
        } else {

            // there is an action context, try to use his value stack
            result = context.getValueStack();

            if (result == null) {

                // no value stack, create then a new one
                ValueStackFactory stackFactory = context.getInstance(ValueStackFactory.class);
                result = stackFactory.createValueStack();
            }
        }
        return result;
    }

    public static <O> XWork2ScopeValidator<O> newXWorkScopeValidator(Class<O> beanClass, String contextName,
                                                                     Set<String> fields) {
        return newXWorkScopeValidator(beanClass, contextName, fields, getSharedValueStack());
    }

    public static <O> XWork2ScopeValidator<O> newXWorkScopeValidator(Class<O> beanClass, String contextName,
                                                                     Set<String> fields, ValueStack vs) {

        return new XWork2ScopeValidator<O>(beanClass, contextName, fields, vs);
    }


    public static String getContextForScope(String context, NuitonValidatorScope scope) {
        return (context == null ? "" : context + "-") + scope.name().toLowerCase();
    }

    protected static ActionValidatorManager newValidationManager(ValueStack vs) {

        if (vs == null) {
            vs = createValuestack();

            if (log.isDebugEnabled()) {
                log.debug("create a standalone value stack " + vs);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("use given value stack " + vs);
            }
        }

        ActionContext context = ActionContext.getContext();

        if (context == null) {
            context = new ActionContext(vs.getContext());

            // must set the action context otherwise can't obtain after validators
            // with the method validator.getValidators(XXX)
            // Later in the code, we could not having reference to the ValueStack
            // before using a validator... Must be cleaned...
            ActionContext.setContext(context);

        }
        Container container = context.getContainer();

        if (context.getActionInvocation() == null) {

            // We need sometimes a ActionInvocation (see http://nuiton.org/issues/2837)

            // ----
            // tchemit-2015-01-06 Don't need this anymore (see http://forge.nuiton.org/issues/3612)

//            Configuration configuration = container.getInstance(Configuration.class);
//
//            UnknownHandlerConfig unknownHandlerStack = new UnknownHandlerConfig("nuiton");
//            List<UnknownHandlerConfig> unknownHandlerStackList = configuration.getUnknownHandlerStack();
//
//            if (unknownHandlerStackList == null) {
//                unknownHandlerStackList = Lists.newArrayList();
//                configuration.setUnknownHandlerStack(unknownHandlerStackList);
//            }
//            unknownHandlerStackList.add(0, unknownHandlerStack);
            // tchemit-2015-01-06 Don't need this anymore (see http://forge.nuiton.org/issues/3612)
            // ----

            Map<String, Object> extraContext = Maps.newHashMap();
            extraContext.put(ActionContext.VALUE_STACK, vs);
            DefaultActionInvocation invocation = new DefaultActionInvocation(extraContext, false);
            invocation.setObjectFactory(container.getInstance(ObjectFactory.class));
            invocation.setContainer(container);

            ActionProxyFactory actionProxyFactory = context.getInstance(ActionProxyFactory.class);
            ActionProxy actionProxy = actionProxyFactory.createActionProxy(invocation, "java.lang", "java.lang.Object", "nuiton-validation", false, false);
            invocation.init(actionProxy);
            context.setActionInvocation(invocation);

        }
        // init validator

        ActionValidatorManager validatorManager =
                container.getInstance(ActionValidatorManager.class, "no-annotations");

        //FIXME-tchemit 2012-07-17 work-around to fix http://nuiton.org/issues/2191
        //FIXME-tchemit 2012-07-17 will remove this when using xworks-core which fixes https://issues.apache.org/jira/browse/WW-3850
//        if (validatorManager instanceof DefaultActionValidatorManager) {
//
//            FileManagerFactory fileManagerFactory = container.getInstance(FileManagerFactory.class);
//            fileManagerFactory.setReloadingConfigs(String.valueOf(Boolean.FALSE));
//            ((DefaultActionValidatorManager) validatorManager).setFileManagerFactory(fileManagerFactory);
//        }

        return validatorManager;
    }

    public static <O> Map<NuitonValidatorScope, String[]> detectFields(Class<O> type, String context,
                                                                       NuitonValidatorScope[] scopeUniverse) {

        Set<String> availableFields = BeanUtil.getReadableProperties(type);

        ActionValidatorManager validatorManager = newValidationManager(getSharedValueStack());

        Map<NuitonValidatorScope, String[]> fields = new TreeMap<NuitonValidatorScope, String[]>();

        for (NuitonValidatorScope scope : scopeUniverse) {

            Set<String> fieldNames =
                    detectFieldsForScope(validatorManager, type, scope, context, availableFields, false);

            if (log.isDebugEnabled()) {
                log.debug("detected validator fields for scope " + scope +
                          ":" + context + " : " + fieldNames);
            }

            if (!fieldNames.isEmpty()) {

                // fields detected in this validator, keep it

                fields.put(scope, fieldNames.toArray(new String[fieldNames.size()]));
            }
        }

        return fields;

    }

    protected static Set<String> detectFieldsForScope(ActionValidatorManager validator,
                                                      Class<?> type,
                                                      NuitonValidatorScope scope,
                                                      String context,
                                                      Set<String> availableFields,
                                                      boolean includeDefaultContext) {

        String scopeContext = getContextForScope(context, scope);

        Set<String> fields = new HashSet<String>();

        int skip = 0;
        if (scopeContext != null && !includeDefaultContext) {
            // count the number of validator to skip
            for (Validator<?> v : validator.getValidators(type, null)) {
                // we only work on FieldValidator at the moment
                if (v instanceof FieldValidator) {
                    skip++;
                }
            }
        }

        for (Validator<?> v : validator.getValidators(type, scopeContext)) {

            // we only work on FieldValidator at the moment
            if (v instanceof FieldValidator) {
                if (skip > 0) {
                    skip--;
                    continue;
                }
                FieldValidator fieldValidator = (FieldValidator) v;
                if (log.isDebugEnabled()) {
                    log.debug("context " + context + " - field " +
                              fieldValidator.getFieldName());
                }
                String fName = fieldValidator.getFieldName();
                if (availableFields.contains(fName)) {

                    // safe field
                    fields.add(fName);
                } else {

                    if (BeanUtil.isNestedReadableProperty(type, fName)) {

                        fields.add(fName);

                    } else {
                        // not a readable property, can not add it
                        String message =
                                "Field " + fName + " in scope [" + scopeContext + "] is not a readable property of " +
                                type.getName();
                        if (log.isErrorEnabled()) {
                            log.error(message);
                        }
                        throw new IllegalStateException(message);
                    }
                }
            }
        }

        return fields;
    }

    /**
     * A dummy unknown handler when we want to use for example visitor validators
     * which need a invocation handler.
     *
     * <strong>Note:</strong> Do not use this for any purpose...
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 3.0
     */
    public static class NuitonDefaultUnknownHandler implements UnknownHandler {

        protected Configuration configuration;

        protected ObjectFactory objectFactory;

        @Inject
        public void setConfiguration(Configuration configuration) {
            this.configuration = configuration;
        }

        @Inject
        public void setObjectFactory(ObjectFactory objectFactory) {
            this.objectFactory = objectFactory;
        }

        @Override
        public ActionConfig handleUnknownAction(String namespace, String actionName) throws XWorkException {
            return new ActionConfig.Builder(namespace, actionName, Object.class.getName()).build();
        }

        @Override
        public Result handleUnknownResult(ActionContext actionContext, String actionName, ActionConfig actionConfig, String resultCode) throws XWorkException {
            return null;
        }

        @Override
        public Object handleUnknownActionMethod(Object action, String methodName) throws NoSuchMethodException {
            return null;
        }
    }
}
