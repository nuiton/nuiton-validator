package org.nuiton.validator.xwork2.field;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator for French SIREN numbers
 *
 * Siret can be in:
 * <ul>
 * <li>String format: "442116703"</li>
 * <li>long, int: 442116703</li>
 * <li>Array or Collection of something: [4,4,2,1,1,6,7,0,,3] or ["442","116","70", "3"]</li>
 * </ul>
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 * @since 3.0
 * Validation do the Luhn checksum too
 */

public class FrenchSirenFieldValidator extends NuitonFieldValidatorSupport {

    protected static final String SIREN_REGEXP = "[0-9]{9}";

    protected static final Pattern p = Pattern.compile(SIREN_REGEXP);

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);

        if (value == null) {
            // no value defined
            return;
        }
        String siren;

        if (value.getClass().isArray()) {
            // le siren est stocker dans un tableau, par exemple un byte[]
            siren = "";
            for (int i = 0; i < Array.getLength(value); i++) {
                siren += String.valueOf(Array.get(value, i));
            }
        } else if (value instanceof Collection<?>) {
            // le siren est stocker dans une collection,
            // ca doit pas arriver souvent :D, mais autant le gerer
            siren = "";
            for (Object o : (Collection<?>) value) {
                siren += String.valueOf(o);
            }
        } else {
            // sinon dans tous les autres cas (String, int, long, BigInteger ...)
            // on prend le toString
            siren = String.valueOf(value);
        }

        if (StringUtils.isEmpty(siren)) {
            // no value defined
            return;
        }

        // Remove any space
        siren = siren.replaceAll(" ", "");

        Matcher m = p.matcher(siren);
        if (!m.matches() || !FieldValidatorUtil.luhnChecksum(siren)) {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "frenchSiret";
    }

}
