/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldExpressionValidator;
import org.nuiton.converter.ConverterUtil;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extends {@link FieldExpressionValidator} to add some extra parameters available
 * in the {@link #getExpression()}
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class FieldExpressionWithParamsValidator extends NuitonFieldExpressionValidator {

    protected static final Pattern EXTRA_BOOLEAN_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+)\\:(false|true)");

    protected static final Pattern EXTRA_SHORT_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+)\\:(-\\d+|\\d+)");

    protected static final Pattern EXTRA_INT_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+)\\:(-\\d+|\\d+)");

    protected static final Pattern EXTRA_LONG_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+)\\:(-\\d+|\\d+)");

    protected static final Pattern EXTRA_DOUBLE_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+)\\:(-\\d+\\.\\d+|\\d+\\.\\d+)");

    protected static final Pattern EXTRA_STRING_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+)\\:(.+)");

    protected ValueStack stack;

    protected String booleanParams;

    protected String shortParams;

    protected String intParams;

    protected String longParams;

    protected String doubleParams;

    protected String stringParams;

    protected Map<String, Boolean> booleans;

    protected Map<String, Short> shorts;

    protected Map<String, Integer> ints;

    protected Map<String, Long> longs;

    protected Map<String, Double> doubles;

    protected Map<String, String> strings;

    public String getBooleanParams() {
        return booleanParams;
    }

    public void setBooleanParams(String booleanParams) {
        this.booleanParams = booleanParams;
    }

    public String getDoubleParams() {
        return doubleParams;
    }

    public void setDoubleParams(String doubleParams) {
        this.doubleParams = doubleParams;
    }

    public String getIntParams() {
        return intParams;
    }

    public void setIntParams(String intParams) {
        this.intParams = intParams;
    }

    public String getLongParams() {
        return longParams;
    }

    public void setLongParams(String longParams) {
        this.longParams = longParams;
    }

    public String getShortParams() {
        return shortParams;
    }

    public void setShortParams(String shortParams) {
        this.shortParams = shortParams;
    }

    public String getStringParams() {
        return stringParams;
    }

    public void setStringParams(String stringParams) {
        this.stringParams = stringParams;
    }

    public Map<String, Boolean> getBooleans() {
        return booleans;
    }

    public Map<String, Double> getDoubles() {
        return doubles;
    }

    public Map<String, Integer> getInts() {
        return ints;
    }

    public Map<String, Long> getLongs() {
        return longs;
    }

    public Map<String, Short> getShorts() {
        return shorts;
    }

    public Map<String, String> getStrings() {
        return strings;
    }

    @Override
    public String getValidatorType() {
        return "fieldexpressionwithparams";
    }

    @Override
    public void setValueStack(ValueStack stack) {
        super.setValueStack(stack);
        this.stack = stack;
    }

    @Override
    public void validate(Object object) throws ValidationException {
        super.validate(object);
    }

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        booleans = initParams(Boolean.class, booleanParams, EXTRA_BOOLEAN_PARAM_ENTRY_PATTERN);
        shorts = initParams(Short.class, shortParams, EXTRA_SHORT_PARAM_ENTRY_PATTERN);
        ints = initParams(Integer.class, intParams, EXTRA_INT_PARAM_ENTRY_PATTERN);
        longs = initParams(Long.class, longParams, EXTRA_LONG_PARAM_ENTRY_PATTERN);
        doubles = initParams(Double.class, doubleParams, EXTRA_DOUBLE_PARAM_ENTRY_PATTERN);
        strings = initParams(String.class, stringParams, EXTRA_STRING_PARAM_ENTRY_PATTERN);

        boolean pop = false;
        if (!stack.getRoot().contains(this)) {
            stack.push(this);
            pop = true;
        }

        try {
            super.validateWhenNotSkip(object);
        } finally {
            if (pop) {
                stack.pop();
            }
        }

    }

    protected <T> Map<String, T> initParams(Class<T> klass, String extraParams, Pattern pattern) throws ValidationException {

        if (extraParams == null || extraParams.isEmpty()) {
            // not using
            return null;
        }

        StringTokenizer stk = new StringTokenizer(extraParams, "|");
        Map<String, T> result = new TreeMap<String, T>();
        while (stk.hasMoreTokens()) {
            String entry = stk.nextToken();
            Matcher matcher = pattern.matcher(entry);
            if (!matcher.matches()) {
                throw new ValidationException("could not parse for extra params " + extraParams + " for type " + klass.getName());
            }
            String paramName = matcher.group(1);
            String paramValueStr = matcher.group(2);
            T paramValue = ConverterUtil.convert(klass, paramValueStr);
            if (log.isDebugEnabled()) {
                log.debug("detected extra param : <type:" + klass + ", name:" + paramName + ", value:" + paramValue + ">");
            }
            result.put(paramName, paramValue);
        }
        return result;
    }
}
