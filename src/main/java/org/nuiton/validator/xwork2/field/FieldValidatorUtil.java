package org.nuiton.validator.xwork2.field;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Util tool for Field validator.
 *
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 * @since 3.0
 */
public class FieldValidatorUtil {

    /**
     * Verifie la validite d'un numero en suivant l'algorithme Luhn tel que d'ecrit
     * dans <a href="http://fr.wikipedia.org/wiki/Luhn">wikipedia</a>
     *
     * Algo:
     * en fonction de la position du numero dans la sequence,
     * on multiplie pas 1 (pour les impaires) ou par 2 pour les paires
     * (1 etant le numero le plus a droite)
     * On fait la somme de tous les chiffres qui resulte de ces multiplications
     * (si un resultat etait 14, on ne fait pas +14 mais +1+4)
     *
     * Si le résultat de cette somme donne un reste de 0 une fois divisé par 10
     * le numero est valide.
     *
     * @param value une chaine composer que de chiffre
     * @return vrai si on a reussi a valider le numero
     */
    public static boolean luhnChecksum(String value) {

        char[] tab = value.toCharArray();
        int sum = 0;

        // on multiple alternativement par 1 ou par 2 les chiffres
        // sachant que le chiffre le plus à droite est multiplié par 1

        // l'offset permet de savoir si le premier chiffre( le plus a gauche) doit etre
        // multiplier par 1 ( offset = 0)
        // multiplier par 2 (offset = 1)
        int offset = (tab.length + 1) % 2;


        for (int i = 0; i < tab.length; i++) {

            // recuperation de la valeur
            int n = getDigit(tab[i]);

            // multiplication par 1 ou 2 en fonction de l'index et de l'offset
            n *= (i + offset) % 2 + 1;

            // si une fois multiplie il est superieur a 9, il faut additionner
            // toutes ces constituante, mais comme il ne peut pas etre superieur
            // a 18, cela revient a retrancher 9
            if (n > 9) {
                n -= 9;
            }

            // on peut directement faire la somme
            sum += n;
        }

        // 3eme phase on verifie que c'est bien un multiple de 10
        boolean result = sum % 10 == 0;

        return result;
    }


    /**
     * Converti un char en un entier '0' =&gt; 0 et '9' =&gt; 9, et 'A' =&gt; 1 a 'Z' =&gt; 36,
     * les autres caractere sont aussi convertis pour que
     * A|B|C|D|E|F|G|H|I|J
     * K|L|M|N|O|P|Q|R|S|T
     * U|V|W|X|Y|Z| | | |
     * -+-+-+-+-+-+-+-+-+
     * 1|2|3|4|5|6|7|8|9|0.
     * Pour les autres c'est un indedermine
     *
     * @param c le caractere qui doit etre converti
     * @return le chiffre
     */
    public static int getDigit(char c) {
        int result = 0;
        if (c >= '0' && c <= '9') {
            result = c - '0';
        } else if (c >= 'A' && c <= 'Z') {
            result = (c - 'A' + 1) % 10;
        } else {
            result = (c - 'a' + 1) % 10;
        }
        return result;
    }
}
