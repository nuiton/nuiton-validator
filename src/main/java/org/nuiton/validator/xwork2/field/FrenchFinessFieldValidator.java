package org.nuiton.validator.xwork2.field;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class FrenchFinessFieldValidator extends NuitonFieldValidatorSupport {

    protected static final String FINESS_REGEXP = "(0[1-9]|[1-9][0-9]|2A|2B)0([0-9]{6})";

    protected static final Pattern p = Pattern.compile(FINESS_REGEXP);

    @Override
    protected void validateWhenNotSkip(Object object) throws ValidationException {
        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);

        if (value == null) {
            // no value defined
            return;
        }
        String finess;

        if (value.getClass().isArray()) {
            // le finess est stocker dans un tableau, par exemple un byte[]
            finess = "";
            for (int i = 0; i < Array.getLength(value); i++) {
                finess += String.valueOf(Array.get(value, i));
            }
        } else if (value instanceof Collection<?>) {
            // le finess est stocker dans une collection,
            // ca doit pas arriver souvent :D, mais autant le gerer
            finess = "";
            for (Object o : (Collection<?>) value) {
                finess += String.valueOf(o);
            }
        } else {
            // sinon dans tous les autres cas (String, int, long, BigInteger ...)
            // on prend le toString
            finess = String.valueOf(value);
        }

        if (StringUtils.isEmpty(finess)) {
            // no value defined
            return;
        }

        // Remove any space
        finess = finess.replaceAll(" ", "");

        Matcher m = p.matcher(finess);
        if (!m.matches() || !FieldValidatorUtil.luhnChecksum(finess)) {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "frenchFiness";
    }
}
