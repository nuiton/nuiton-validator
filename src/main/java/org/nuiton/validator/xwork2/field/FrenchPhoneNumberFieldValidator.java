/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator used to validate French phone numbers like :
 * - 0000000000
 * - 00.00.00.00.00
 * - 00-00-00-00-00
 * - 00 00 00 00 00
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class FrenchPhoneNumberFieldValidator extends NuitonFieldValidatorSupport {

    protected static String PHONE_NUMBER_REGEXP =
            "[0-9]{10}|(([0-9]{2}[-\\.\\s]){4})[0-9]{2}";

    protected static Pattern p = Pattern.compile(PHONE_NUMBER_REGEXP);

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);

        if (value == null) {
            // no value defined
            return;
        }
        if (value instanceof String) {
            if ("".equals(value)) {
                // no value defined
                return;
            }
            Matcher m = p.matcher((String) value);
            if (!m.matches()) {
                addFieldError(fieldName, object);
            }
        } else {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "frenchPhoneNumber";
    }
}
