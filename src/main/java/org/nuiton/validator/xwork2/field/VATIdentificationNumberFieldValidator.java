/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator for EU VAT number.
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class VATIdentificationNumberFieldValidator extends NuitonFieldValidatorSupport {

    protected static String VAT_INTRA_REGEXP = "^(RO\\d{2,10}|GB\\d{5}|(ATU|DK|FI|HU|LU|MT|CZ|SI)\\d{8}|IE[A-Z\\d]{8}|(DE|BG|EE|EL|LT|BE0|PT|CZ)\\d{9}|CY\\d{8}[A-Z]|(ES|GB)[A-Z\\d]{9}|(BE0|PL|SK|CZ)\\d{10}|(FR|IT|LV)\\d{11}|(LT|SE)\\d{12}|(NL|GB)[A-Z\\d]{12})$";

    protected static Pattern p = Pattern.compile(VAT_INTRA_REGEXP);

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);

        if (value == null) {
            // no value defined
            return;
        }
        if (value instanceof String) {

            String vatNumber = (String) value;
            if (StringUtils.isEmpty(vatNumber)) {
                // no value defined
                return;
            }

            // Remove any space
            vatNumber = vatNumber.replaceAll(" ", "");

            Matcher m = p.matcher(vatNumber);
            if (!m.matches()) {
                addFieldError(fieldName, object);
            }
        } else {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "VATIdentificationNumber";
    }
}
