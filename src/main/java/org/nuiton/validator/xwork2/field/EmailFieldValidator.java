/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.StringUtil;

/**
 * Validator for email addresses :
 * - Deal with + in addresses
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 */
public class EmailFieldValidator extends NuitonFieldValidatorSupport {

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);

        if (value == null) {
            // no value defined
            return;
        }
        if (value instanceof String) {
            if (StringUtils.isEmpty((String) value)) {
                // no value defined
                return;
            }
            if (!StringUtil.isEmail((String) value)) {
                addFieldError(fieldName, object);
            }
        } else {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "emailNuiton";
    }
}
