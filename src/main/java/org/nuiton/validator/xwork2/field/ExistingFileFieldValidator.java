/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import com.opensymphony.xwork2.validator.ValidationException;

import java.io.File;

/**
 * <!-- START SNIPPET: javadoc -->
 * ExistingFileFieldValidator checks that a File field exists. *
 * <!-- END SNIPPET: javadoc -->
 * <!-- START SNIPPET: parameters -->
 * <ul>
 * <li>fieldName - The field name this validator is validating. Required if using Plain-Validator Syntax otherwise not required</li>
 * </ul>
 * <!-- END SNIPPET: parameters -->
 * <pre>
 * <!-- START SNIPPET: examples -->
 *     &lt;validators&gt;
 *         &lt;!-- Plain-Validator Syntax --&gt;
 *         &lt;validator type="fileExisting"&gt;
 *             &lt;param name="fieldName"&gt;tmp&lt;/param&gt;
 *             &lt;message&gt;tmp is not an existing file&lt;/message&gt;
 *         &lt;/validator&gt;
 *
 *         &lt;!-- Field-Validator Syntax --&gt;
 *         &lt;field name="tmp"&gt;
 *         	  &lt;field-validator type="fileExisting"&gt;
 *                 &lt;message&gt;tmp is not an existing file&lt;/message&gt;
 *            &lt;/field-validator&gt;
 *         &lt;/field&gt;
 *     &lt;/validators&gt;
 * <!-- END SNIPPET: examples -->
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ExistingFileFieldValidator extends NuitonFieldValidatorSupport {

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {
        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);
        if (value == null) {
            // no value defined
            addFieldError(fieldName, object);
            return;
        }
        File f;
        if (value instanceof File) {
            f = (File) value;
        } else if (value instanceof String) {
            f = new File((String) value);
        } else {
            addFieldError(fieldName, object);
            return;
        }

        if (!(f.isFile() && f.exists())) {
            // f is  not a file nor exists
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "existingFile";
    }
}
