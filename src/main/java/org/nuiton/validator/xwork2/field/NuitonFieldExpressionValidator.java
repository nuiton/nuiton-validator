/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2.field;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldExpressionValidator;

/**
 * Nuiton default field validator.
 *
 * This validator offers a {@link #skip} property that can be used to skip or
 * not the validator, this property is a OGNL expression.
 *
 * To use this new field validator support, just now implements the method
 * {@link #validateWhenNotSkip(Object)}. This method will be invoked only if the skip
 * parameter is evaluated to {@code false}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3
 */
public class NuitonFieldExpressionValidator extends FieldExpressionValidator {

    /**
     * extra parameter at the very beginning of the
     * {@link #validate(Object)} method to be able to skip (or not) the
     * validator execution.
     *
     * by default the value is {@code false} : it seems fair to want to
     * validate if the validator is used :D...
     */
    protected String skip = "false";

    /**
     * Sets the value of the {@link #skip} parameter.
     *
     * @param skip the new value of the {@link #skip} parameter
     */
    public void setSkip(String skip) {
        this.skip = skip;
    }

    /**
     * Method to be invoked when skip parameter was not evaludated to {@code true}.
     *
     * @param object the object to be validated.
     * @throws ValidationException is thrown if there is validation error(s).
     */

    protected void validateWhenNotSkip(Object object) throws ValidationException {
        super.validate(object);
    }

    @Override
    public void validate(Object object) throws ValidationException {

        // evaluate the skip parameter
        boolean mustSkip = evaluateSkipParameter(object);

        if (mustSkip) {

            // skip is set to true, so skip the validation
            if (log.isDebugEnabled()) {
                log.debug("Skip the validation from " + this +
                          ", due to skip parameter evaluated to true");
            }
            return;
        }

        // must validate
        validateWhenNotSkip(object);
    }

    /**
     * Evaluate the skip parameter value against the object to validate.
     *
     * This parameter can be an OGNL expression.
     *
     * @param object the object to validate
     * @return the evaluation of the skip parameter.
     * @throws ValidationException if could not evaluate the parameter
     */
    protected boolean evaluateSkipParameter(Object object) throws ValidationException {

        skip = skip.trim();

        if ("false".equals(skip)) {

            return false;
        }

        if ("true".equals(skip)) {

            return true;
        }

        try {
            Boolean answer = Boolean.FALSE;
            Object obj;
            obj = getFieldValue(skip, object);
            if (obj != null && obj instanceof Boolean) {
                answer = (Boolean) obj;
            }
            return answer;
        } catch (ValidationException e) {
            throw e;
        } catch (Exception e) {
            // let this pass, but it will be logged right below
            throw new ValidationException(
                    "Can not evaluate boolean expression [" + skip +
                    "] for reason " + e.getMessage());
        }

    }

}
