/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2;

import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.NuitonValidatorResult;
import org.nuiton.validator.NuitonValidatorScope;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of {@link NuitonValidator} using {@code XWork2} validators.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class XWork2NuitonValidator<O> implements NuitonValidator<O> {

    protected NuitonValidatorModel<O> model;

    /** xworks scope validators * */
    protected Map<NuitonValidatorScope, XWork2ScopeValidator<O>> validators;

    public XWork2NuitonValidator(NuitonValidatorModel<O> model) {

        this.model = model;

        // init validators
        validators = new EnumMap<NuitonValidatorScope, XWork2ScopeValidator<O>>(NuitonValidatorScope.class);

        Class<O> type = model.getType();
        String context = model.getContext();

        Map<NuitonValidatorScope, String[]> fieldsMap = model.getFields();

        for (Map.Entry<NuitonValidatorScope, String[]> entry : fieldsMap.entrySet()) {

            NuitonValidatorScope scope = entry.getKey();

            String scopeContext =
                    XWork2ValidatorUtil.getContextForScope(
                            context,
                            scope
                    );

            Set<String> fields = new HashSet<String>(
                    Arrays.asList(entry.getValue()));

            XWork2ScopeValidator<O> newValidator =
                    XWork2ValidatorUtil.newXWorkScopeValidator(
                            type,
                            scopeContext,
                            fields
                    );

            validators.put(scope, newValidator);
        }
    }

    @Override
    public NuitonValidatorResult validate(O object) throws NullPointerException {

        if (object == null) {
            throw new NullPointerException("object parameter can not be null.");
        }

        NuitonValidatorResult result = new NuitonValidatorResult();

        for (NuitonValidatorScope scope : validators.keySet()) {

            XWork2ScopeValidator<O> validator = validators.get(scope);

            Map<String, List<String>> newMessages = validator.validate(object);

            result.addMessagesForScope(scope, newMessages);
        }
        return result;
    }

    @Override
    public Set<NuitonValidatorScope> getEffectiveScopes() {
        return validators.keySet();
    }

    @Override
    public Set<String> getEffectiveFields() {
        Set<String> result = new HashSet<String>();
        for (XWork2ScopeValidator<O> scopeValidator : validators.values()) {
            result.addAll(scopeValidator.getFieldNames());
        }
        return result;
    }

    @Override
    public Set<String> getEffectiveFields(NuitonValidatorScope scope) {
        Set<String> result = new HashSet<String>();
        XWork2ScopeValidator<O> scopeValidator = validators.get(scope);
        if (scopeValidator != null) {
            result.addAll(scopeValidator.getFieldNames());
        }
        return result;
    }

    @Override
    public NuitonValidatorModel<O> getModel() {
        return model;
    }
}
