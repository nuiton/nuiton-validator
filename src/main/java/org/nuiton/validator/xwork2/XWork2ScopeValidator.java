/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2013 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.validator.xwork2;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ActionValidatorManager;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A customized validator for a given bean.
 *
 * Use the method {@link #validate(Object)} to obtain the messages detected by
 * the validator for the given bean.
 *
 * @param <O> type of the bean to validate.
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class XWork2ScopeValidator<O> {

    /** Logger */
    private static final Log log = LogFactory.getLog(XWork2ScopeValidator.class);

    protected final static Map<String, List<String>> EMPTY_RESULT =
            Collections.unmodifiableMap(new HashMap<String, List<String>>());

    /** the type of bean to validate */
    protected final Class<O> type;

    /** the validation named context (can be null) */
    protected String context;

    /** the list of field names detected for this validator */
    protected Set<String> fieldNames;

    // --
    // XWorks fields
    // --

    protected ValidationAwareSupport validationSupport;

    protected DelegatingValidatorContext validationContext;

    protected ActionValidatorManager validator;

    protected ValueStack vs;

    protected XWork2ScopeValidator(Class<O> type,
                                   String context,
                                   Set<String> fieldNames,
                                   ValueStack vs) {

        this.type = type;
        this.context = context;
        this.fieldNames = fieldNames;

        validationSupport = new ValidationAwareSupport();
        validationContext = new DelegatingValidatorContext(validationSupport);

        if (vs == null) {

            // create a standalone value stack
            vs = XWork2ValidatorUtil.createValuestack();
            if (log.isDebugEnabled()) {
                log.debug("create a standalone value stack " + vs);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("use given value stack " + vs);
            }
        }

        this.vs = vs;

        validator = XWork2ValidatorUtil.newValidationManager(vs);
    }

    public Class<O> getType() {
        return type;
    }

    public String getContext() {
        return context;
    }

    public Set<String> getFieldNames() {
        return fieldNames;
    }

    public ActionValidatorManager getValidator() {
        return validator;
    }

    /**
     * Test if the validator contains the field given his name
     *
     * @param fieldName the name of the searched field
     * @return <code>true</code> if validator contaisn this field,
     * <code>false</code> otherwise
     */
    public boolean containsField(String fieldName) {
        return fieldNames.contains(fieldName);
    }

    /**
     * Valide le bean donné et retourne les messages produits.
     *
     * @param bean le bean a valider (il doit etre non null)
     * @return le dictionnaire des messages produits par la validation indexées
     * par le nom du champs du bean impacté.
     */
    public Map<String, List<String>> validate(O bean) {

        if (bean == null) {
            throw new NullPointerException(
                    "bean parameter can not be null in method validate");
        }

        Map<String, List<String>> result = EMPTY_RESULT;


        if (fieldNames.isEmpty()) {
            return result;
        }

        // on lance la validation uniquement si des champs sont a valider
        try {

            //TC - 20081024 : since context is in a ThreadLocal variable,
            // we must do the check
            if (ActionContext.getContext() == null) {
                ActionContext.setContext(new ActionContext(vs.getContext()));
            }

            validator.validate(bean, context, validationContext);

            if (log.isTraceEnabled()) {
                log.trace("Action errors: " +
                          validationContext.getActionErrors());
                log.trace("Action messages: " +
                          validationContext.getActionMessages());
                log.trace("Field errors: " +
                          validationContext.getFieldErrors());
            }

            if (log.isDebugEnabled()) {
                log.debug(this + " : " +
                          validationContext.getFieldErrors());
            }

            // retreave errors by field
            if (validationContext.hasFieldErrors()) {
                Map<?, ?> messages = validationContext.getFieldErrors();
                result = new HashMap<String, List<String>>(messages.size());
                for (Object fieldName : messages.keySet()) {
                    Collection<?> c =
                            (Collection<?>) messages.get(fieldName);
                    List<String> mm = new ArrayList<String>(c.size());
                    for (Object message : c) {
                        // tchemit 2010-08-28 : trim the incoming message
                        // (I18n will not translate it otherwise)
                        String messageStr = message == null ? "" : message + "";
                        mm.add(messageStr.trim());
                    }
                    result.put(fieldName + "", mm);
                }
            }

        } catch (ValidationException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Error during validation on " + type +
                         " for reason : " + eee.getMessage(), eee);
            }

        } finally {

            // on nettoye toujours le validateur apres operation
            validationSupport.clearErrorsAndMessages();
        }

        return result;
    }

    @Override
    public String toString() {
        return super.toString() + "<beanClass:" + type +
               ", contextName:" + context + ">";
    }

}
